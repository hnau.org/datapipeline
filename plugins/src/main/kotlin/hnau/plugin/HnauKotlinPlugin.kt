package hnau.plugin

import hnau.plugin.utils.setJavaVersionCompatibility
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

class HnauKotlinPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        target.plugins.apply {
            apply("org.jetbrains.kotlin.jvm")
        }
        target.setJavaVersionCompatibility()
        target.tasks.withType(KotlinCompile::class.java) { kotlinCompile ->
            kotlinCompile.kotlinOptions.freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
            kotlinCompile.kotlinOptions.freeCompilerArgs += "-Xcontext-receivers"
        }
        target.tasks.withType(Test::class.java) { test ->
            test.useJUnitPlatform()
        }
    }
}
