package hnau.plugin.utils

import org.gradle.api.JavaVersion
import org.gradle.api.artifacts.VersionCatalog

fun VersionCatalog.requireVersion(
    alias: String,
): String = findVersion(alias)
    .get()
    .requiredVersion

fun VersionCatalog.getTargetJvmVersion(): JavaVersion =
    requireVersion("java").let(JavaVersion::valueOf)
