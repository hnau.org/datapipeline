package hnau.plugin.utils

import org.gradle.api.plugins.ExtensionContainer

inline fun <reified Extension> ExtensionContainer.get(): Extension =
    getByType(Extension::class.java)

inline fun <reified Extension> ExtensionContainer.find(): Extension? =
    findByType(Extension::class.java)

inline fun <reified Extension> ExtensionContainer.configure(
    crossinline configureAction: Extension.() -> Unit,
) {
    configure(Extension::class.java) { extension -> configureAction.invoke(extension) }
}
