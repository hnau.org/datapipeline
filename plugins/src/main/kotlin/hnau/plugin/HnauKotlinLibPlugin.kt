package hnau.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project

class HnauKotlinLibPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        target.plugins.apply {
            apply("hnau.kotlin")
        }
    }
}
