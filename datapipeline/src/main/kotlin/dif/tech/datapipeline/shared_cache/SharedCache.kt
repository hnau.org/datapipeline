package dif.tech.datapipeline.shared_cache

interface SharedCache {

  fun <T> getCachedValue(
    factory: SharedCacheValueFactory<T>,
  ): T
}
