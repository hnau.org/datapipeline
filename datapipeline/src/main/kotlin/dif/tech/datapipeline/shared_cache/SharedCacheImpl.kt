package dif.tech.datapipeline.shared_cache

import kotlinx.coroutines.CoroutineScope

class SharedCacheImpl(
    private val scope: CoroutineScope,
) : SharedCache {

    private val values = HashMap<Class<SharedCacheValueFactory<*>>, Any?>()

    @Suppress("UNCHECKED_CAST")
    @Synchronized
    override fun <T> getCachedValue(
        factory: SharedCacheValueFactory<T>,
    ): T = values
        .getOrPut(factory.javaClass) {
            factory.createImmortalCacheValue(scope)
        }
            as T
}
