package dif.tech.datapipeline.shared_cache

import kotlinx.coroutines.CoroutineScope

fun interface SharedCacheValueFactory<T> {

  fun createImmortalCacheValue(
    cacheScope: CoroutineScope,
  ): T
}
