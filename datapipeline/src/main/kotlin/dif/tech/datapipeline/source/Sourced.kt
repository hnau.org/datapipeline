package dif.tech.datapipeline.source

data class Sourced<out T>(
  val source: Source,
  val value: T,
)

fun <T> T.toSourced(
  source: Source,
): Sourced<T> = Sourced(
  source = source,
  value = this,
)

fun <T> T.toSourcedNew(): Sourced<T> =
  toSourced(Source.New)

fun <T> T.toSourcedCached(): Sourced<T> =
  toSourced(Source.Cached)
