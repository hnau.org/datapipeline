package dif.tech.datapipeline.source

enum class Source { New, Cached, Foreign, }
