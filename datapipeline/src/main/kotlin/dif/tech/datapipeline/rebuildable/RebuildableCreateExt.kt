package dif.tech.datapipeline.rebuildable

import dif.tech.common.kotlin.scopedInState
import dif.tech.common.kotlin.state.mapStateLite
import dif.tech.common.kotlin.state.cacheStateInScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update

@Suppress("FunctionName")
inline fun <T> Rebuildable(
    scope: CoroutineScope,
    crossinline build: (CoroutineScope, version: Int) -> T,
): Rebuildable<T> = object : Rebuildable<T> {

    private val stateVersion = MutableStateFlow(0)

    override val state: StateFlow<T> = stateVersion
        .scopedInState(scope)
        .mapStateLite { (stateScope, version) -> build(stateScope, version) }
        .cacheStateInScope(scope)

    override fun rebuild() {
        stateVersion.update { it + 1 }
    }
}
