package dif.tech.datapipeline.rebuildable

import dif.tech.common.kotlin.state.mapStateLite
import dif.tech.common.kotlin.state.cacheStateInScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

fun <I, O> Rebuildable<I>.map(
    scope: CoroutineScope,
    transform: (I) -> O,
): Rebuildable<O> = object : Rebuildable<O> {

    private val source: Rebuildable<I> = this@map

    override val state: StateFlow<O> = source
        .state
        .mapStateLite(transform)
        .cacheStateInScope(scope)

    override fun rebuild() {
        source.rebuild()
    }
}
