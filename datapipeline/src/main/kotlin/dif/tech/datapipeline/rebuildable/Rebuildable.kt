package dif.tech.datapipeline.rebuildable

import kotlinx.coroutines.flow.StateFlow

interface Rebuildable<T> {

  val state: StateFlow<T>

  fun rebuild()
}
