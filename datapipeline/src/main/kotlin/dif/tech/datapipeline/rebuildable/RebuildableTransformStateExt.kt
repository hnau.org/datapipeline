package dif.tech.datapipeline.rebuildable

import kotlinx.coroutines.flow.StateFlow

inline fun <I, O> Rebuildable<I>.transformState(
  transform: (StateFlow<I>) -> StateFlow<O>,
): Rebuildable<O> {
  val source: Rebuildable<I> = this@transformState
  val newState: StateFlow<O> = transform(source.state)
  return object : Rebuildable<O> {

    override val state: StateFlow<O> get() = newState

    override fun rebuild() {
      source.rebuild()
    }
  }
}
