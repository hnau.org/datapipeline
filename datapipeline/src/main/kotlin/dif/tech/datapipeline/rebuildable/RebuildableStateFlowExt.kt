package dif.tech.datapipeline.rebuildable

import dif.tech.common.kotlin.state.flatMapStateLite
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

fun <T> Rebuildable<StateFlow<T>>.flatten(): Rebuildable<T> = object : Rebuildable<T> {

    private val source: Rebuildable<StateFlow<T>> = this@flatten

    override val state: StateFlow<T> = source.state.flatMapStateLite { it }

    override fun rebuild() {
        source.rebuild()
    }
}

@Suppress("FunctionName")
inline fun <T> StateFlowRebuildable(
    scope: CoroutineScope,
    crossinline build: (CoroutineScope, version: Int) -> StateFlow<T>,
): Rebuildable<T> = Rebuildable(
    scope = scope,
    build = build,
).flatten()
