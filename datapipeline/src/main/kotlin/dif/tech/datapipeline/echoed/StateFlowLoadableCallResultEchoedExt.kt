package dif.tech.datapipeline.echoed

import arrow.core.None
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.fold
import dif.tech.common.plata.BaseDomainError
import dif.tech.common.plata.ResourceResult
import dif.tech.common.plata.toOption
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow

fun <R, E : BaseDomainError> StateFlow<Loadable<ResourceResult<R, E>>>.echoedLoadableCallResult(
    scope: CoroutineScope,
): StateFlow<Echoed<Loadable<ResourceResult<R, E>>, R>> = echoed(
    scope = scope,
) { newValue ->
    newValue.fold(
        ifLoading = { None },
        ifReady = ResourceResult<R, E>::toOption,
    )
}
