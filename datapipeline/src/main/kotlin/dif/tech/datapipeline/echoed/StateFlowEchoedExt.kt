package dif.tech.datapipeline.echoed

import arrow.core.Option
import dif.tech.common.kotlin.state.runningFoldState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow

inline fun <T, E> StateFlow<T>.echoed(
    scope: CoroutineScope,
    crossinline valueToEchoValue: (T) -> Option<E>,
): StateFlow<Echoed<T, E>> = runningFoldState(
    scope = scope,
    createInitial = { initialValue ->
        Echoed(
            value = initialValue,
            echoValue = valueToEchoValue(initialValue),
        )
    },
) { previous, current ->
    previous.next(
        newValue = current,
        newEchoValue = valueToEchoValue(current),
    )
}
