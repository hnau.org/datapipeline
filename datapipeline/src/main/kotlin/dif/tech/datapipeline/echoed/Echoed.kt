package dif.tech.datapipeline.echoed

import arrow.core.None
import arrow.core.Option
import arrow.core.Some


data class Echoed<out T, out E>(
  val value: T,
  val echoValue: Option<E>,
)

fun <T, E> Echoed<T, E>.next(
  newValue: T,
  newEchoValue: Option<E>,
): Echoed<T, E> = Echoed(
  value = newValue,
  echoValue = when (newEchoValue) {
    None -> echoValue
    is Some -> newEchoValue
  },
)
