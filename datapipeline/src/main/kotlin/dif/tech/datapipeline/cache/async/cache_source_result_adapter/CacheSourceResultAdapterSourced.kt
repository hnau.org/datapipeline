package dif.tech.datapipeline.cache.async.cache_source_result_adapter

import dif.tech.common.kotlin.Tagged
import dif.tech.datapipeline.source.Source
import dif.tech.datapipeline.source.Sourced
import dif.tech.datapipeline.source.toSourced

fun <CACHE, SOURCE, RESULT, TAG> CacheSourceResultAdapter<CACHE, SOURCE, RESULT>.sourced(
    cachedTag: TAG,
    instanceTag: TAG,
): CacheSourceResultAdapter<Tagged<CACHE, TAG>, SOURCE, Sourced<RESULT>> = CacheSourceResultAdapter(
    cacheToResult = { cache, isInitialValue ->
        val (value, valueTag) = cache
        val valueSource = when (valueTag) {
            instanceTag -> Source.New
            cachedTag -> Source.Cached
            else -> when (isInitialValue) {
                true -> Source.Cached
                false -> Source.Foreign
            }
        }
        cacheToResult(value, isInitialValue).map { it.toSourced(valueSource) }
    },
    sourceToNotCacheableResultOrCache = { source ->
        sourceToNotCacheableResultOrCache(source)
            ?.map { notCacheableResult ->
                Tagged(notCacheableResult, instanceTag)
            }
            ?.mapLeft { valueToCache ->
                Sourced(Source.New, valueToCache)
            }
    }
)

fun <CACHE, SOURCE, RESULT> CacheSourceResultAdapter<CACHE, SOURCE, Sourced<RESULT>>.filterResultSources(
    availableSources: Set<Source>,
): CacheSourceResultAdapter<CACHE, SOURCE, Sourced<RESULT>> = filterResult { (source) ->
    source in availableSources
}

fun <CACHE, SOURCE, RESULT> CacheSourceResultAdapter<CACHE, SOURCE, Sourced<RESULT>>.filterResultSources(
    vararg availableSources: Source,
): CacheSourceResultAdapter<CACHE, SOURCE, Sourced<RESULT>> = filterResultSources(
    availableSources = availableSources.toSet(),
)

fun <T, TAG> CacheSourceResultAdapter.Companion.sourced(
    cachedTag: TAG,
    instanceTag: TAG,
): CacheSourceResultAdapter<Tagged<T, TAG>, T, Sourced<T>> =
    pipe<T>().sourced(cachedTag, instanceTag)