package dif.tech.datapipeline.cache

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import kotlinx.coroutines.flow.MutableStateFlow

internal fun <T> MutableStateFlow<T>.compareAndSet(
    expect: Option<T>,
    value: T,
): Boolean = when (expect) {
    None -> {
        this.value = value
        true
    }

    is Some -> compareAndSet(expect.value, value)
}