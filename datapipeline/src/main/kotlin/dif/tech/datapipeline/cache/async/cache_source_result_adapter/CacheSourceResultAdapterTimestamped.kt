package dif.tech.datapipeline.cache.async.cache_source_result_adapter

import dif.tech.common.kotlin.Timestamped

fun <CACHE, SOURCE, RESULT> CacheSourceResultAdapter<CACHE, SOURCE, RESULT>.timestamped(): CacheSourceResultAdapter<Timestamped<CACHE>, SOURCE, Timestamped<RESULT>> =
    CacheSourceResultAdapter(
        cacheToResult = { (cache, cacheTimestamp), isInitialValue ->
            cacheToResult(cache, isInitialValue).map { Timestamped(it, cacheTimestamp) }
        },
        sourceToNotCacheableResultOrCache = { source ->
            sourceToNotCacheableResultOrCache(source)
                ?.map(Timestamped.Companion::now)
                ?.mapLeft(Timestamped.Companion::now)
        }
    )

fun <CACHE, SOURCE, RESULT> CacheSourceResultAdapter<CACHE, SOURCE, Timestamped<RESULT>>.clearTimestamp(): CacheSourceResultAdapter<CACHE, SOURCE, RESULT> =
    CacheSourceResultAdapter(
        cacheToResult = { cache, isInitialValue ->
            cacheToResult(cache, isInitialValue).map { it.value }
        },
        sourceToNotCacheableResultOrCache = { source ->
            sourceToNotCacheableResultOrCache(source)?.mapLeft { (value) -> value }
        },
    )

fun <T> CacheSourceResultAdapter.Companion.timestamped(): CacheSourceResultAdapter<Timestamped<T>, T, Timestamped<T>> =
    pipe<T>().timestamped()