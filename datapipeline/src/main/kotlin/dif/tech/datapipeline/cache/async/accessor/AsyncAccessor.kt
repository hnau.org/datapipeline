package dif.tech.datapipeline.cache.async.accessor

import arrow.core.Option


interface AsyncAccessor<T> {

  suspend fun read(): Option<T>

  suspend fun write(value: T)

  companion object {

    inline operator fun <T> invoke(
      crossinline read: suspend () -> Option<T>,
      crossinline write: suspend (T) -> Unit,
    ): AsyncAccessor<T> = object : AsyncAccessor<T> {

      override suspend fun read(): Option<T> = read.invoke()

      override suspend fun write(value: T) {
        write.invoke(value)
      }
    }
  }
}
