package dif.tech.datapipeline.cache.async

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.flatMap
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.CacheSourceResultAdapter
import dif.tech.datapipeline.cache.async.collect_source_waiter.CollectSourceWaiter
import dif.tech.datapipeline.cache.async.collect_source_waiter.immediate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.transformLatest

fun <CACHE, SOURCE, RESULT> AsyncCache<CACHE>.withSource(
    scope: CoroutineScope,
    source: Flow<SOURCE>,
    adapter: CacheSourceResultAdapter<CACHE, SOURCE, RESULT>,
    collectSourceWaiter: CollectSourceWaiter<CACHE> = CollectSourceWaiter.immediate(),
): StateFlow<Loadable<RESULT>> {

    fun cacheValueToSourceValue(
        cacheValue: Loadable<Option<CACHE>>,
        initial: Boolean,
    ): Loadable<RESULT> = cacheValue.flatMap { optionValue ->
        optionValue.fold(
            ifEmpty = { Loadable.Loading },
            ifSome = { valueFromCache ->
                when (val value = adapter.cacheToResult(valueFromCache, initial)) {
                    None -> Loadable.Loading
                    is Some -> Loadable.Ready(value.value)
                }
            },
        )
    }

    val (initialValueFromCache, initialCacheValue) = value
        .value
        .let { initialCachedValue ->
            initialCachedValue to cacheValueToSourceValue(
                cacheValue = initialCachedValue,
                initial = true,
            )
        }

    val resultsFromCache: Flow<RESULT> = value
        .map { valueFromCache ->
            if (valueFromCache === initialValueFromCache) {
                return@map initialCacheValue
            }
            cacheValueToSourceValue(
                cacheValue = valueFromCache,
                initial = false,
            )
        }
        .filterIsInstance<Loadable.Ready<RESULT>>()
        .map { it.value }

    val nonCacheableResultsFromSource: Flow<RESULT> = flow {
        collectSourceWaiter.waitCacheToCollectSource(value)
        source
            .map(adapter::sourceToNotCacheableResultOrCache)
            .transformLatest { notCacheableResultOrCache ->
                when (notCacheableResultOrCache) {
                    is Either.Left -> emit(notCacheableResultOrCache.value)
                    is Either.Right -> update(notCacheableResultOrCache.value)
                    null -> {}
                }
            }
            .collect(this)
    }

    return merge(resultsFromCache, nonCacheableResultsFromSource)
        .map { value -> Loadable.Ready(value) }
        .stateIn(
            scope = scope,
            initialValue = initialCacheValue,
            started = SharingStarted.Eagerly,
        )
}
