package dif.tech.datapipeline.cache.async.collect_source_waiter

import arrow.core.Option
import dif.tech.common.kotlin.Loadable
import kotlinx.coroutines.flow.StateFlow

fun interface CollectSourceWaiter<in T> {

    suspend fun waitCacheToCollectSource(
        valuesFromCache: StateFlow<Loadable<Option<T>>>,
    )

    companion object
}