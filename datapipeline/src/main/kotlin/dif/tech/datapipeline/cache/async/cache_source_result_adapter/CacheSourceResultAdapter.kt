package dif.tech.datapipeline.cache.async.cache_source_result_adapter

import arrow.core.Either
import arrow.core.Option

interface CacheSourceResultAdapter<CACHE, in SOURCE, out RESULT> {

    fun cacheToResult(cache: CACHE, isInitialValue: Boolean): Option<RESULT>

    fun sourceToNotCacheableResultOrCache(source: SOURCE): Either<RESULT, CACHE>?

    companion object {

        inline operator fun <CACHE, SOURCE, RESULT> invoke(
            crossinline cacheToResult: (cache: CACHE, isInitialValue: Boolean) -> Option<RESULT>,
            crossinline sourceToNotCacheableResultOrCache: (source: SOURCE) -> Either<RESULT, CACHE>?,
        ): CacheSourceResultAdapter<CACHE, SOURCE, RESULT> = object : CacheSourceResultAdapter<CACHE, SOURCE, RESULT> {

            override fun cacheToResult(cache: CACHE, isInitialValue: Boolean): Option<RESULT> =
                cacheToResult.invoke(cache, isInitialValue)

            override fun sourceToNotCacheableResultOrCache(source: SOURCE): Either<RESULT, CACHE>? =
                sourceToNotCacheableResultOrCache.invoke(source)
        }
    }
}
