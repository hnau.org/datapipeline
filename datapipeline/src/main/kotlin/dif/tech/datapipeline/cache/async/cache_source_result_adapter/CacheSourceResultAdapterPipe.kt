package dif.tech.datapipeline.cache.async.cache_source_result_adapter

import arrow.core.Either
import arrow.core.Some

fun <T> CacheSourceResultAdapter.Companion.pipe(): CacheSourceResultAdapter<T, T, T> = CacheSourceResultAdapter(
    cacheToResult = { cache, _ -> Some(cache) },
    sourceToNotCacheableResultOrCache = { Either.Right(it) }
)