package dif.tech.datapipeline.cache.async

import arrow.core.None
import arrow.core.Option
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.LoadableStateFlow
import dif.tech.common.kotlin.fold
import dif.tech.common.kotlin.map
import dif.tech.common.kotlin.scopedInState
import dif.tech.common.kotlin.state.cacheStateInScope
import dif.tech.common.kotlin.state.flatMapStateLite
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

suspend inline fun <T> AsyncCache<T>.update(
    updater: (Loadable<Option<T>>) -> T,
) {
    var updated: Boolean
    do {
        val previousValue = value.value
        val newValue = updater(previousValue)
        updated = update(
            value = newValue,
            expectedActualValue = previousValue,
        )
    } while (!updated)
}

inline fun <I, O> AsyncCache<I>.map(
    scope: CoroutineScope,
    crossinline direct: suspend (I) -> Option<O>,
    crossinline reverse: suspend (O) -> I,
): AsyncCache<O> = object : AsyncCache<O> {

    private val source: AsyncCache<I> get() = this@map

    override val value: StateFlow<Loadable<Option<O>>> = source
        .value
        .scopedInState(scope)
        .flatMapStateLite { (scope, sourceValue) ->
            sourceValue.fold(
                ifLoading = { MutableStateFlow(Loadable.Loading) },
                ifReady = { sourceReadyValue ->
                    sourceReadyValue.fold(
                        ifEmpty = { MutableStateFlow(Loadable.Ready(None)) },
                        ifSome = { value -> LoadableStateFlow(scope) { direct(value) } },
                    )
                },
            )
        }
        .cacheStateInScope(scope)

    override suspend fun update(
        value: O,
        expectedActualValue: Loadable<Option<O>>?,
    ): Boolean {
        val transformedValue = reverse(value)
        val transformedExpectedActualValue = expectedActualValue?.map { expectedOrNone ->
            expectedOrNone.map { expected ->
                reverse(expected)
            }
        }
        return source.update(
            value = transformedValue,
            expectedActualValue = transformedExpectedActualValue,
        )
    }
}
