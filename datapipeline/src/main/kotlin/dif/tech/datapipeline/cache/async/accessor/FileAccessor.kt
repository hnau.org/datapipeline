package dif.tech.datapipeline.cache.async.accessor

import arrow.core.toOption
import dif.tech.common.kotlin.mapper.Mapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

fun <T> FileAccessor(
    file: File,
    stringMapper: Mapper<String, T>,
): AsyncAccessor<T> = AsyncAccessor(
    read = {
        withContext(Dispatchers.IO) {
            file.takeIf { it.exists() }?.readText()?.let(stringMapper.direct).toOption()
        }
    },
    write = { value ->
        withContext(Dispatchers.IO) {
            file.writeText(stringMapper.reverse(value))
        }
    }
)