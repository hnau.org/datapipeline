package dif.tech.datapipeline.cache.async.collect_source_waiter

fun <T> CollectSourceWaiter.Companion.immediate(): CollectSourceWaiter<T> =
    CollectSourceWaiter {}