package dif.tech.datapipeline.cache.sync

import arrow.core.Option
import kotlinx.coroutines.flow.StateFlow

interface SyncCache<T> {

    val value: StateFlow<Option<T>>

    fun update(
        value: T,
        expectedActualValue: Option<T>? = null,
    ): Boolean

    companion object
}
