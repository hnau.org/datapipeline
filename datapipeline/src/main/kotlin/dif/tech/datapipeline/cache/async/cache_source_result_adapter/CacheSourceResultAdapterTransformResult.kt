package dif.tech.datapipeline.cache.async.cache_source_result_adapter

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.left

inline fun <CACHE, SOURCE, RESULT_I, RESULT_O> CacheSourceResultAdapter<CACHE, SOURCE, RESULT_I>.transformResult(
    crossinline transform: (RESULT_I) -> Option<RESULT_O>,
): CacheSourceResultAdapter<CACHE, SOURCE, RESULT_O> = CacheSourceResultAdapter(
    cacheToResult = { cache, isInitialValue ->
        cacheToResult(cache, isInitialValue).flatMap(transform)
    },
    sourceToNotCacheableResultOrCache = { source ->
        sourceToNotCacheableResultOrCache(source)?.fold(
            ifLeft = { resultI ->
                transform(resultI)
                    .left()
                    .getOrNull()
            },
            ifRight = { Either.Right(it) }
        )
    }
)

inline fun <CACHE, SOURCE, RESULT> CacheSourceResultAdapter<CACHE, SOURCE, RESULT>.filterResult(
    crossinline predicate: (RESULT) -> Boolean,
): CacheSourceResultAdapter<CACHE, SOURCE, RESULT> = transformResult { result ->
    when (predicate(result)) {
        true -> Some(result)
        false -> None
    }
}