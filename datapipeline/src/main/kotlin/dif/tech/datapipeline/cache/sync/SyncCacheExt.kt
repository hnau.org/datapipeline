package dif.tech.datapipeline.cache.sync

import arrow.core.Option
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.state.cacheStateInScope
import dif.tech.common.kotlin.state.mapStateLite
import dif.tech.datapipeline.cache.async.AsyncCache
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

fun <T> SyncCache<T>.toAsync(): AsyncCache<T> = object : AsyncCache<T> {

    private val source: SyncCache<T> get() = this@toAsync

    override val value: StateFlow<Loadable<Option<T>>> =
        source.value.mapStateLite { syncValue -> Loadable.Ready(syncValue) }

    override suspend fun update(
        value: T,
        expectedActualValue: Loadable<Option<T>>?,
    ): Boolean = source.update(
        value = value,
        expectedActualValue = when (expectedActualValue) {
            null, Loadable.Loading -> null
            is Loadable.Ready -> expectedActualValue.value
        }
    )
}

inline fun <I, O> SyncCache<I>.map(
    scope: CoroutineScope,
    crossinline direct: (I) -> Option<O>,
    crossinline reverse: (O) -> I,
): SyncCache<O> = object : SyncCache<O> {

    private val source: SyncCache<I> get() = this@map

    override val value: StateFlow<Option<O>> = source
        .value
        .mapStateLite { sourceValue ->
            sourceValue.flatMap { value -> direct(value) }
        }
        .cacheStateInScope(scope)

    override fun update(
        value: O,
        expectedActualValue: Option<O>?,
    ): Boolean = source.update(
        value = reverse(value),
        expectedActualValue = expectedActualValue?.map(reverse),
    )
}
