package dif.tech.datapipeline.cache.async.collect_source_waiter

import arrow.core.None
import arrow.core.Some
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.Time
import dif.tech.common.kotlin.Timestamped
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull

fun <T> CollectSourceWaiter.Companion.timeDelta(
    maxDeltaToSubscribeToSource: Time,
    extractTimestamp: (T) -> Time,
): CollectSourceWaiter<T> = CollectSourceWaiter {
    val timeToWait = it
        .mapNotNull { loadableValueOrNone ->
            when (loadableValueOrNone) {
                Loadable.Loading -> null
                is Loadable.Ready -> when (val valueOrNone = loadableValueOrNone.value) {
                    None -> Time.zero
                    is Some -> {
                        val value = valueOrNone.value
                        val timestamp = extractTimestamp(value)
                        timestamp + maxDeltaToSubscribeToSource - Time.getNow()
                    }
                }
            }
        }
        .first()
    delay(timeToWait.milliseconds)
}

fun <T> CollectSourceWaiter.Companion.timeDelta(
    maxDeltaToSubscribeToSource: Time,
): CollectSourceWaiter<Timestamped<T>> = timeDelta(
    maxDeltaToSubscribeToSource = maxDeltaToSubscribeToSource,
    extractTimestamp = Timestamped<T>::timestamp,
)