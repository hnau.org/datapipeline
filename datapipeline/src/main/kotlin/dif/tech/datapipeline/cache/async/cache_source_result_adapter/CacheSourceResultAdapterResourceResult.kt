package dif.tech.datapipeline.cache.async.cache_source_result_adapter

import arrow.core.Either
import dif.tech.common.plata.BaseDomainError
import dif.tech.common.plata.DomainError
import dif.tech.common.plata.ResourceResult
import dif.tech.common.plata.SystemError
import dif.tech.common.plata.mapResult

fun <CACHE, SOURCE, RESULT, E : BaseDomainError> CacheSourceResultAdapter<CACHE, SOURCE, RESULT>.resourceResult() =
    CacheSourceResultAdapter<CACHE, ResourceResult<SOURCE, E>, ResourceResult<RESULT, E>>(
        cacheToResult = { cache, isInitialValue ->
            cacheToResult(cache, isInitialValue).map { ResourceResult.Result(it) }
        },
        sourceToNotCacheableResultOrCache = { source ->
            source.mapResult(
                successMapper = { sourceResult ->
                    this
                        .sourceToNotCacheableResultOrCache(sourceResult)
                        ?.mapLeft { notCacheableResult ->
                            ResourceResult.Result(notCacheableResult)
                        }
                },
                systemErrorMapper = { error ->
                    Either.Left(SystemError(error))
                },
                domainErrorMapper = { error ->
                    Either.Left(DomainError(error))
                },
            )
        }
    )

fun <T, E : BaseDomainError> CacheSourceResultAdapter.Companion.resourceResult(): CacheSourceResultAdapter<T, ResourceResult<T, E>, ResourceResult<T, E>> =
    pipe<T>().resourceResult()
