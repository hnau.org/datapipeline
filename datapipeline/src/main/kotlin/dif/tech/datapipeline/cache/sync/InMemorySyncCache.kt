package dif.tech.datapipeline.cache.sync

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.toOption
import dif.tech.datapipeline.cache.compareAndSet
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

fun <T> SyncCache.Companion.inMemory(): SyncCache<T> = object : SyncCache<T> {

    private val value_: MutableStateFlow<Option<T>> =
        MutableStateFlow(None)

    override val value: StateFlow<Option<T>>
        get() = value_

    override fun update(
        value: T,
        expectedActualValue: Option<T>?,
    ): Boolean = value_.compareAndSet(
        expect = expectedActualValue.toOption(),
        value = Some(value),
    )
}
