package dif.tech.datapipeline.cache.async.accessor

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.identity
import arrow.core.toOption
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.Tagged
import dif.tech.common.kotlin.Time
import dif.tech.common.kotlin.Timestamped
import dif.tech.datapipeline.cache.async.AsyncCache
import dif.tech.datapipeline.cache.compareAndSet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

inline fun <I, O> AsyncAccessor<I>.transform(
    crossinline transformRead: suspend (I) -> Option<O>,
    crossinline transformWrite: suspend (O) -> I,
): AsyncAccessor<O> = AsyncAccessor(
    read = { read().flatMap { transformRead(it) } },
    write = { write(transformWrite(it)) }
)

inline fun <I, O> AsyncAccessor<I>.map(
    crossinline mapRead: suspend (I) -> O,
    crossinline mapWrite: suspend (O) -> I,
): AsyncAccessor<O> = transform(
    transformRead = { Some(mapRead(it)) },
    transformWrite = mapWrite,
)

fun <T, TAG> AsyncAccessor<T>.tagged(
    defaultTag: TAG,
): AsyncAccessor<Tagged<T, TAG>> = map(
    mapRead = { Tagged(it, defaultTag) },
    mapWrite = { (value, _) -> value },
)

inline fun <T> AsyncAccessor<T>.filter(
    crossinline predicate: (T) -> Boolean,
): AsyncAccessor<T> = transform(
    transformRead = {
        when (predicate(it)) {
            true -> Some(it)
            false -> None
        }
    },
    transformWrite = ::identity,
)

fun <T> AsyncAccessor<Timestamped<T>>.applyLifetime(
    lifetime: Time,
): AsyncAccessor<Timestamped<T>> = filter {(_, timestamp) ->
    Time.getNow() - timestamp <= lifetime
}

fun <T> AsyncAccessor<T>.toCache(
    scope: CoroutineScope,
): AsyncCache<T> = object : AsyncCache<T> {

    private val readInitialValueJobAccessMutex = Mutex()

    private val updateMutex = Mutex()

    private var readInitialValueJob: Job?
        set(value) {
            field?.cancel()
            field = value
        }

    private val value_: MutableStateFlow<Loadable<Option<T>>> =
        MutableStateFlow(Loadable.Loading)

    init {
        readInitialValueJob = scope.launch {
            val initialValue = read()
            trySetInitialValue(initialValue)
        }
    }

    private suspend fun trySetInitialValue(
        initialValue: Option<T>,
    ) {
        readInitialValueJobAccessMutex.withLock {
            if (readInitialValueJob != null) {
                value_.value = Loadable.Ready(initialValue)
            }
        }
    }

    override val value: StateFlow<Loadable<Option<T>>>
        get() = value_

    override suspend fun update(
        value: T,
        expectedActualValue: Loadable<Option<T>>?,
    ): Boolean = updateMutex.withLock {

        val valueToSet = Loadable.Ready(Some(value))

        val localUpdated = value_.compareAndSet(
            expect = expectedActualValue.toOption(),
            value = valueToSet,
        )

        if (localUpdated) {
            readInitialValueJobAccessMutex.withLock { readInitialValueJob = null }
            write(value)
        }

        localUpdated
    }
}
