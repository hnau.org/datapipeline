package dif.tech.datapipeline.cache.async

import arrow.core.Option
import dif.tech.common.kotlin.Loadable
import kotlinx.coroutines.flow.StateFlow

interface AsyncCache<T> {

    val value: StateFlow<Loadable<Option<T>>>

    suspend fun update(
        value: T,
        expectedActualValue: Loadable<Option<T>>? = null,
    ): Boolean

    companion object
}
