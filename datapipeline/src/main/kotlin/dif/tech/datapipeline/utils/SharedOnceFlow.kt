package dif.tech.datapipeline.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.shareIn

@Suppress("FunctionName")
fun <T> SharedOnceFlow(
    scope: CoroutineScope,
    block: suspend () -> T,
): Flow<T> {
    val sharedFlow: Flow<T> = flow {
        val value = block()
        emit(value)
    }.shareIn(
        scope = scope,
        started = SharingStarted.WhileSubscribed(),
    )
    return flow {
        val value = sharedFlow.first()
        emit(value)
    }
}