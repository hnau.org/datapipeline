package dif.tech.datapipeline.utils

data class ArgumentedHolder<K, V>(
  private val create: (K) -> V,
) {

  private val values = HashMap<K, V>()

  @Synchronized
  operator fun get(
    key: K,
  ): V = values.getOrPut(key) {
    create(key)
  }

  inline fun <O> map(
    crossinline transform: (K, V) -> O,
  ): ArgumentedHolder<K, O> = ArgumentedHolder { key ->
    val value = get(key)
    transform(key, value)
  }
}
