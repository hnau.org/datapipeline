plugins {
    id("hnau.kotlin.lib")
}

dependencies {
    implementation(libs.arrow.core)
    implementation(libs.arrow.coroutines)
    implementation(libs.kotlin.coroutines.core)
    implementation(libs.kotlin.serialization.core)
    implementation(libs.kotlin.serialization.json)
    implementation(project(mapOf("path" to ":common:kotlin")))
    implementation(project(mapOf("path" to ":common:plata")))
}
