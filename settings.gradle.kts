rootProject.name = "DataPipeline"

pluginManagement {
    includeBuild("plugins")
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

fun collectSubproject(
    projectDir: File,
    projectIdentifier: String = "",
): List<String> {
    fun collectProjectWithSubprojects(
        dir: File,
        projectIdentifier: String,
    ): List<String> {
        fun collectProjectWithoutSubprojects(
            dir: File,
            projectIdentifier: String,
        ): List<String> {
            if (projectIdentifier == ":plugins") {
                return emptyList()
            }
            if (projectIdentifier == ":buildSrc") {
                return emptyList()
            }
            val isProject = dir
                .list()
                ?.any { file -> file == "settings.gradle.kts" || file == "build.gradle.kts" }
                ?: false
            if (!isProject) {
                return emptyList()
            }
            return listOf(projectIdentifier)
        }

        return collectProjectWithoutSubprojects(dir, projectIdentifier) + collectSubproject(
            dir,
            projectIdentifier,
        )
    }

    return projectDir
        .listFiles()
        .orEmpty()
        .filter { it.exists() && it.isDirectory }
        .flatMap { subdir ->
            val subdirName = subdir.name
            collectProjectWithSubprojects(
                dir = subdir,
                projectIdentifier = "$projectIdentifier:$subdirName",
            )
        }
}

include(collectSubproject(rootProject.projectDir))
