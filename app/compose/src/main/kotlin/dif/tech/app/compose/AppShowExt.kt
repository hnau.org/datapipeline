package dif.tech.app.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import dif.tech.app.app.App
import dif.tech.app.compose.localizer.LocalLocalizer
import dif.tech.app.compose.localizer.Localizer
import dif.tech.app.compose.localizer.LocalizerImpl
import dif.tech.app.compose.model.Show
import dif.tech.app.compose.utils.DataPipelineTheme

@Composable
fun App.Show(
    localizer: Localizer = LocalizerImpl,
) {
    CompositionLocalProvider(LocalLocalizer provides localizer) {
        DataPipelineTheme {
            model.Show()
        }
    }
}