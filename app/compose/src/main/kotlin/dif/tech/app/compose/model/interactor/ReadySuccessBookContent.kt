package dif.tech.app.compose.model.interactor

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import dif.tech.app.app.model.interactor.value.Author
import dif.tech.app.compose.localizer.localizer
import dif.tech.app.compose.model.color
import dif.tech.app.compose.model.title
import dif.tech.common.compose.uikit.utils.Dimens
import dif.tech.datapipeline.source.Source

@Composable
internal fun ReadySuccessBookContent(
    author: Author,
    book: LoadingOrReadyBook.Ready.ReadyState.Success,
) = Column(
    modifier = Modifier.fillMaxSize(),
    verticalArrangement = Arrangement.spacedBy(Dimens.extraSmallSeparation),
) {
    AnimatedContent(
        targetState = book.lastLoadingErrorMessage,
        modifier = Modifier
            .weight(1f)
            .fillMaxWidth(),
    ) { lastLoadingErrorMessage ->
        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.BottomStart,
        ) {
            Text(
                text = localizer.interactor.status + ": " + when (lastLoadingErrorMessage) {
                    null -> localizer.interactor.actual
                    else -> localizer.interactor.lastSucces +
                            ". " +
                            localizer.interactor.lastLoadingError +
                            ": " +
                            lastLoadingErrorMessage
                },
                style = MaterialTheme.typography.caption,
                color = when (lastLoadingErrorMessage) {
                    null -> MaterialTheme.colors.primary
                    else -> MaterialTheme.colors.error
                },
                maxLines = 1,
            )
        }
    }
    AnimatedContent(
        targetState = book.book,
        modifier = Modifier
            .weight(2.5f)
            .fillMaxWidth(),
    ) { book ->
        Column(
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.spacedBy(
                space = Dimens.extraSmallSeparation,
                alignment = Alignment.Top,
            )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
            ) {
                Text(
                    text = book.value.title,
                    style = MaterialTheme.typography.h5,
                    color = MaterialTheme.colors.onSurface,
                    maxLines = 1,
                )
                Text(
                    text = "(" + localizer.interactor.name + " " + author.title + ")",
                    style = MaterialTheme.typography.h5,
                    color = author.color.copy(alpha = 0.5f),
                    maxLines = 1,
                )
            }
            Text(
                text = localizer.interactor.source + ": " + when (book.source) {
                    Source.New -> localizer.interactor.new
                    Source.Cached -> localizer.interactor.cache
                    Source.Foreign -> localizer.interactor.foreign
                },
                style = MaterialTheme.typography.caption,
                color = MaterialTheme.colors.onSurface.copy(alpha = 0.5f),
                maxLines = 1,
            )
        }
    }
}