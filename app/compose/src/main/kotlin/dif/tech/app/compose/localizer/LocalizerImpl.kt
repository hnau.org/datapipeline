package dif.tech.app.compose.localizer

internal data object LocalizerImpl : Localizer {

    override val root: Localizer.Root = object : Localizer.Root {
        override val noInteractors: String = "There are no interactors"
        override val addInteractor: String = "Add interactor"
    }

    override val interactor: Localizer.Interactor = object : Localizer.Interactor {
        override val loadingBook: String = "Loading book"
        override val reload: String = "Reload"
        override val source: String = "Source"
        override val new: String = "new"
        override val cache: String = "cache"
        override val foreign: String = "foreign"
        override val error: String = "Error"
        override val status: String = "Status"
        override val actual: String = "Actual"
        override val lastSucces: String = "Last success"
        override val lastLoadingError: String = "Last loading error"
        override val chooseAuthor: String = "Choose author"
        override val name: String = "Name"
    }
}