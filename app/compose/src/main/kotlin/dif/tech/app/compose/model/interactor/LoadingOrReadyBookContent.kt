package dif.tech.app.compose.model.interactor

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dif.tech.app.app.model.interactor.value.Author

@Composable
internal fun LoadingOrReadyBookContent(
    author: Author,
    book: LoadingOrReadyBook,
    isLoading: Boolean,
) = AnimatedContent(
    targetState = book,
    contentKey = { it is LoadingOrReadyBook.Ready },
    modifier = Modifier.fillMaxSize(),
) { localBook ->
    when (localBook) {
        LoadingOrReadyBook.Loading -> LoadingBookContent()
        is LoadingOrReadyBook.Ready -> ReadyBookContent(
            author = author,
            book = localBook,
            isLoading = isLoading,
        )
    }
}