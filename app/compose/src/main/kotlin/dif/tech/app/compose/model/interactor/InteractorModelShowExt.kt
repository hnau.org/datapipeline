package dif.tech.app.compose.model.interactor

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dif.tech.app.app.model.interactor.InteractorModel
import dif.tech.app.compose.model.color
import dif.tech.common.compose.uikit.utils.Dimens

@Composable
internal fun InteractorModel.Show() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(96.dp)
            .padding(start = Dimens.separation),
        verticalAlignment = Alignment.CenterVertically,
    ) {

        Box(
            modifier = Modifier
                .fillMaxHeight()
                .width(48.dp),
            contentAlignment = Alignment.CenterStart,
        ) {
            Text(
                text = index.toString(),
                color = author.color,
                style = MaterialTheme.typography.h5,
                maxLines = 1,
            )
        }


        IconButton(
            onClick = ::onRefreshClick,
            modifier = Modifier.padding(end = Dimens.smallSeparation),
        ) {
            Icon(
                imageVector = Icons.Default.Refresh,
                contentDescription = null,
            )
        }

        val currentBook by book.collectAsState()
        val loadingOrReadyBook = remember(currentBook) { LoadingOrReadyBook(currentBook) }
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f),
        ) {
            LoadingOrReadyBookContent(
                author = author,
                book = loadingOrReadyBook,
                isLoading = isLoading.collectAsState().value,
            )
        }

        IconButton(
            onClick = onCloseClick,
        ) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = null,
            )
        }
    }
}