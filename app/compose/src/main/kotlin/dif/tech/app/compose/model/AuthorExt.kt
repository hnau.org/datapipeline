package dif.tech.app.compose.model

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import dif.tech.app.app.model.interactor.value.Author
import dif.tech.common.compose.uikit.color.MaterialColors
import dif.tech.common.compose.uikit.color.blue
import dif.tech.common.compose.uikit.color.orange

internal val Author.title: String
    @Composable
    get() = name

internal val Author.color: Color
    @Composable
    get() = when (this) {
        Author.A -> MaterialColors.orange
        Author.B -> MaterialColors.blue
    }.v500