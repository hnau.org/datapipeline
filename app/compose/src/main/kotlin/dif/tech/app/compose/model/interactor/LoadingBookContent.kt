package dif.tech.app.compose.model.interactor

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import dif.tech.app.compose.localizer.localizer
import dif.tech.common.compose.uikit.progressindicator.ProgressIndicator
import dif.tech.common.compose.uikit.utils.Dimens

@Composable
internal fun LoadingBookContent() = Row(
    modifier = Modifier.fillMaxSize(),
    horizontalArrangement = Arrangement.spacedBy(
        space = Dimens.smallSeparation,
        alignment = Alignment.CenterHorizontally,
    ),
    verticalAlignment = Alignment.CenterVertically,
) {
    ProgressIndicator(modifier = Modifier.scale(0.5f))
    Text(
        text = localizer.interactor.loadingBook,
        color = MaterialTheme.colors.onSurface,
        style = MaterialTheme.typography.body1,
        maxLines = 1,
    )
}