package dif.tech.app.compose.model.interactor

import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import dif.tech.app.compose.localizer.localizer
import dif.tech.common.compose.uikit.utils.Dimens

@Composable
internal fun ReadyErrorBookContent(
    book: LoadingOrReadyBook.Ready.ReadyState.Error,
) {
    AnimatedContent(
        targetState = book.message,
        modifier = Modifier.fillMaxSize(),
    ) { message ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = Dimens.separation),
            contentAlignment = Alignment.Center,
        ) {
            Text(
                text = localizer.interactor.error + ": " + message,
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.body1,
                maxLines = 1,
            )
        }
    }
}