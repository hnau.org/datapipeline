package dif.tech.app.compose.localizer

import androidx.compose.runtime.Composable
import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.staticCompositionLocalOf

internal val LocalLocalizer: ProvidableCompositionLocal<Localizer> =
    staticCompositionLocalOf { error("CompositionLocal Localizer not present") }

val localizer: Localizer
    @Composable
    get() = LocalLocalizer.current