package dif.tech.app.compose.model.interactor

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import dif.tech.app.app.model.interactor.value.Author
import dif.tech.common.compose.uikit.progressindicator.ProgressIndicator

@Composable
internal fun ReadyBookContent(
    author: Author,
    book: LoadingOrReadyBook.Ready,
    isLoading: Boolean,
) = Row(
    modifier = Modifier.fillMaxSize(),
    verticalAlignment = Alignment.CenterVertically,
) {
    AnimatedVisibility(
        visible = isLoading,
        modifier = Modifier.fillMaxHeight(),
    ) {
        Box(
            modifier = Modifier.fillMaxHeight(),
            contentAlignment = Alignment.Center,
        ) {
            ProgressIndicator(
                modifier = Modifier.scale(0.5f),
            )
        }
    }
    AnimatedContent(
        targetState = book.state,
        modifier = Modifier
            .fillMaxHeight()
            .weight(1f),
        contentKey = { it is LoadingOrReadyBook.Ready.ReadyState.Success },
    ) { state ->
        when (state) {
            is LoadingOrReadyBook.Ready.ReadyState.Error -> ReadyErrorBookContent(state)
            is LoadingOrReadyBook.Ready.ReadyState.Success -> ReadySuccessBookContent(
                author = author,
                book = state,
            )
        }
    }
}