package dif.tech.app.compose.model.interactor

import arrow.core.None
import arrow.core.Some
import dif.tech.app.app.model.interactor.value.Book
import dif.tech.common.kotlin.Loadable
import dif.tech.common.plata.ResourceResult
import dif.tech.common.plata.SimpleDomainError
import dif.tech.datapipeline.echoed.Echoed
import dif.tech.datapipeline.source.Source
import dif.tech.datapipeline.source.Sourced

internal sealed interface LoadingOrReadyBook {

    data object Loading : LoadingOrReadyBook

    data class Ready(
        val state: ReadyState,
    ) : LoadingOrReadyBook {

        sealed interface ReadyState {

            data class Success(
                val book: Sourced<Book>,
                val lastLoadingErrorMessage: String?,
            ) : ReadyState

            data class Error(
                val message: String,
            ) : ReadyState
        }
    }

    companion object {

        private fun Echoed<*, Sourced<Book>>.tryExtractBook(
            lastLoadingErrorMessage: String?,
        ): LoadingOrReadyBook? = when (val value = echoValue) {
            None -> null
            is Some -> Ready(
                state = Ready.ReadyState.Success(
                    book = value.value,
                    lastLoadingErrorMessage = lastLoadingErrorMessage,
                ),
            )
        }

        operator fun invoke(
            book: Echoed<Loadable<ResourceResult<Sourced<Book>, SimpleDomainError>>, Sourced<Book>>,
        ): LoadingOrReadyBook = when (val currentLoadable = book.value) {
            Loadable.Loading -> book.tryExtractBook(
                lastLoadingErrorMessage = null,
            ) ?: Loading

            is Loadable.Ready -> {
                when (val bookOrError = currentLoadable.value) {
                    is ResourceResult.Result -> Ready(
                        state = Ready.ReadyState.Success(
                            book = bookOrError.result,
                            lastLoadingErrorMessage = null,
                        )
                    )

                    is ResourceResult.Error -> {
                        val message = bookOrError.message ?: "Unknown error"
                        book.tryExtractBook(
                            lastLoadingErrorMessage = message,
                        ) ?: Ready(
                            state = Ready.ReadyState.Error(message)
                        )
                    }
                }
            }
        }
    }
}