package dif.tech.app.compose.localizer

interface Localizer {

    interface Root {
        val noInteractors: String
        val addInteractor: String
    }
    val root: Root

    interface Interactor {
        val loadingBook: String
        val reload: String
        val source: String
        val new: String
        val cache: String
        val foreign: String
        val status: String
        val actual: String
        val lastSucces: String
        val lastLoadingError: String
        val error: String
        val chooseAuthor: String
        val name: String
    }
    val interactor: Interactor
}