package dif.tech.app.compose.model

import ErrorPanel
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import arrow.core.Option
import arrow.core.toNonEmptyListOrNull
import dif.tech.app.app.model.AppModel
import dif.tech.app.app.model.interactor.value.Author
import dif.tech.app.compose.localizer.localizer
import dif.tech.app.compose.model.interactor.Show
import dif.tech.common.compose.common.AnimatedVisibility
import dif.tech.common.compose.uikit.chip.Chip
import dif.tech.common.compose.uikit.chip.ChipStyle
import dif.tech.common.compose.uikit.shape.HnauShape
import dif.tech.common.compose.uikit.utils.Dimens

@OptIn(ExperimentalFoundationApi::class, ExperimentalLayoutApi::class)
@Composable
internal fun AppModel.Show() = Box(
    modifier = Modifier.fillMaxSize(),
) {
    Column(
        modifier = Modifier.fillMaxSize(),
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
        ) {
            val currentInteractors by interactors.collectAsState()
            AnimatedContent(
                targetState = currentInteractors.toNonEmptyListOrNull(),
                modifier = Modifier.fillMaxSize(),
                contentKey = { interactorsOrNull -> interactorsOrNull != null },
            ) { interactorsOrNull ->
                when (interactorsOrNull) {
                    null -> ErrorPanel(
                        title = {
                            Text(
                                text = localizer.root.noInteractors,
                                maxLines = 1,
                            )
                        },
                        button = {
                            Chip(
                                onClick = ::onCreateNewInteractorClick,
                                content = {
                                    Text(
                                        text = localizer.root.addInteractor,
                                        maxLines = 1,
                                    )
                                },
                            )
                        },
                    )

                    else -> LazyColumn(
                        modifier = Modifier.fillMaxSize(),
                        contentPadding = PaddingValues(
                            start = Dimens.separation,
                            end = Dimens.separation,
                            top = Dimens.separation,
                            bottom = Dimens.fabSeparationVertical + Dimens.separation,
                        ),
                        verticalArrangement = Arrangement.spacedBy(Dimens.separation),
                    ) {
                        items(
                            items = interactorsOrNull,
                            key = { it.index },
                        ) { model ->
                            Card(
                                modifier = Modifier
                                    .animateItemPlacement()
                                    .fillParentMaxWidth(),
                            ) {
                                model.Show()
                            }
                        }
                    }
                }
            }
            FloatingActionButton(
                onClick = ::onCreateNewInteractorClick,
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(Dimens.separation),
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = null,
                )
            }
        }
        AnimatedVisibility(
            value = newBookRequestCallback.collectAsState().value,
            toLocal = Option.Companion::fromNullable,
            modifier = Modifier.fillMaxWidth(),
        ) { callback ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(56.dp)
                    .background(MaterialTheme.colors.surface)
                    .padding(horizontal = Dimens.separation),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(Dimens.separation),
            ) {
                Text(
                    text = "New book request",
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.onSurface,
                    maxLines = 1,
                    modifier = Modifier.weight(1f),
                )

                Chip(
                    onClick = { callback.complete(AppModel.NewBookRequestResult.Error) },
                    style = ChipStyle.builder.transparentBackground.transparentBorder.activeForeground,
                    content = { Text("Error") },
                    leading = {
                        Icon(
                            imageVector = Icons.Default.Close,
                            contentDescription = null,
                        )
                    },
                    activeColor = MaterialTheme.colors.error,
                )

                Chip(
                    onClick = { callback.complete(AppModel.NewBookRequestResult.Success) },
                    style = ChipStyle.chipHighlighted,
                    content = { Text("Success") },
                    leading = {
                        Icon(
                            imageVector = Icons.Default.Done,
                            contentDescription = null,
                        )
                    },
                    activeColor = MaterialTheme.colors.primary,
                )
            }
        }
    }
    androidx.compose.animation.AnimatedVisibility(
        visible = isAuthorChooseDialogVisible.collectAsState().value,
        modifier = Modifier.fillMaxSize(),
        enter = fadeIn(),
        exit = fadeOut(),
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = null,
                    onClick = ::onAuthorChooseDialogDismiss,
                )
                .background(MaterialTheme.colors.background.copy(alpha = 0.5f))
                .padding(Dimens.separation),
            contentAlignment = Alignment.Center,
        ) {
            Column(
                modifier = Modifier
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = null,
                        onClick = {},
                    )
                    .background(
                        color = MaterialTheme.colors.surface,
                        shape = HnauShape(),
                    )
                    .padding(Dimens.separation),
                verticalArrangement = Arrangement.spacedBy(
                    space = Dimens.separation,
                    alignment = Alignment.CenterVertically,
                ),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = localizer.interactor.chooseAuthor,
                    color = MaterialTheme.colors.onSurface,
                    style = MaterialTheme.typography.subtitle1,
                )
                FlowRow(
                    horizontalArrangement = Arrangement.spacedBy(
                        space = Dimens.smallSeparation,
                        alignment = Alignment.CenterHorizontally,
                    ),
                ) {
                    Author.entries.forEach { author ->
                        key(author) {
                            Chip(
                                onClick = { onAuthorChoose(author) },
                                style = ChipStyle.chipSelected,
                                activeColor = author.color,
                                content = {
                                    Text(
                                        text = localizer.interactor.name + " " + author.title,
                                    )
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

