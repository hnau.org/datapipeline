package dif.tech.app.compose.utils

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

internal val dataPipelineColors: Colors
    @Composable get() = when (isSystemInDarkTheme()) {
        true -> nightDataPipelineColors
        false -> dayDataPipelineColors
    }


private val dayDataPipelineColors: Colors
    @Composable get() = lightColors(
        primary = Color(0xff43A047),
        primaryVariant = Color(0xff2E7D32),
        onPrimary = Color(0xfff7f7f7),
        secondary = Color(0xffFFB300),
        secondaryVariant = Color(0xffFF8F00),
        onSecondary = Color(0xfff7f7f7),
        surface = Color(0xfffafafa),
        onSurface = Color(0xff101010),
        error = Color(0xffe53935),
        onError = Color(0xff212121),
        background = Color(0xfff0f0f0),
        onBackground = Color(0xff101010),
    )

private val nightDataPipelineColors: Colors
    @Composable get() = darkColors(
        primary = Color(0xff66BB6A),
        primaryVariant = Color(0xff43A047),
        onPrimary = Color(0xfff7f7f7),
        secondary = Color(0xffFFCA28),
        secondaryVariant = Color(0xffFFB300),
        onSecondary = Color(0xff101010),
        surface = Color(0xff101010),
        onSurface = Color(0xfff7f7f7),
        error = Color(0xffc62828),
        onError = Color(0xfff7f7f7),
        background = Color(0xff090909),
        onBackground = Color(0xfff7f7f7),
    )
