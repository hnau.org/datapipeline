plugins {
    id("hnau.kotlin.lib")
    alias(libs.plugins.compose.desktop)
}


dependencies {
    implementation(compose.desktop.currentOs)
    implementation(libs.kotlin.coroutines.core)
    implementation(libs.arrow.core)
    implementation(project(mapOf("path" to ":app:app")))
    implementation(project(mapOf("path" to ":common:compose:uikit")))
    implementation(project(mapOf("path" to ":common:compose:common")))
    implementation(project(mapOf("path" to ":common:kotlin")))
    implementation(project(mapOf("path" to ":datapipeline")))
    implementation(project(mapOf("path" to ":common:plata")))
}