package dif.tech.app.app.model.interactor.value

import dif.tech.common.kotlin.Tagged
import dif.tech.common.kotlin.Timestamped
import dif.tech.common.plata.ResourceResult
import dif.tech.common.plata.SimpleDomainError
import dif.tech.datapipeline.cache.async.AsyncCache
import kotlinx.coroutines.flow.Flow
import java.util.UUID

data class SharedValue(
    val cache: AsyncCache<Tagged<Timestamped<Book>, UUID>>,
    val loader: Flow<ResourceResult<Book, SimpleDomainError>>,
)