package dif.tech.app.app

import dif.tech.app.app.model.AppModel
import dif.tech.app.app.model.AppSkeletonSerializable
import dif.tech.common.kotlin.mapper.toMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.serialization.json.Json

class App(
    scope: CoroutineScope,
    savedState: String?,
) {

    private val json = Json {
        ignoreUnknownKeys = true
        prettyPrint = true
    }

    private val modelSkeletonMapper =
        json.toMapper(AppSkeletonSerializable.serializer())

    private val modelSkeleton = savedState
        ?.let(modelSkeletonMapper.direct)
        ?: AppSkeletonSerializable()

    val model = AppModel(
        modelScope = scope,
        skeleton = modelSkeleton,
    )

    val savableState: String
        get() = modelSkeletonMapper.reverse(modelSkeleton)
}