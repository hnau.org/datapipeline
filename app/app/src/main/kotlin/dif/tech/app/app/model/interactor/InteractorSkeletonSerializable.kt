package dif.tech.app.app.model.interactor

import dif.tech.app.app.model.interactor.value.Author
import kotlinx.serialization.Serializable

@Serializable
class InteractorSkeletonSerializable(
    override val author: Author,
    override val index: Int,
) : InteractorSkeleton