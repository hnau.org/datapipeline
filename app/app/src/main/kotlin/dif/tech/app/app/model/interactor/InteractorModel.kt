package dif.tech.app.app.model.interactor

import dif.tech.app.app.model.interactor.value.Author
import dif.tech.app.app.model.interactor.value.Book
import dif.tech.app.app.model.interactor.value.SharedValue
import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.Timestamped
import dif.tech.common.kotlin.mapper.toMapper
import dif.tech.common.kotlin.minutes
import dif.tech.common.kotlin.seconds
import dif.tech.common.plata.ResourceResult
import dif.tech.common.plata.SimpleDomainError
import dif.tech.datapipeline.cache.async.accessor.FileAccessor
import dif.tech.datapipeline.cache.async.accessor.applyLifetime
import dif.tech.datapipeline.cache.async.accessor.tagged
import dif.tech.datapipeline.cache.async.accessor.toCache
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.CacheSourceResultAdapter
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.clearTimestamp
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.filterResultSources
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.resourceResult
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.sourced
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.timestamped
import dif.tech.datapipeline.cache.async.collect_source_waiter.CollectSourceWaiter
import dif.tech.datapipeline.cache.async.collect_source_waiter.immediate
import dif.tech.datapipeline.cache.async.collect_source_waiter.timeDelta
import dif.tech.datapipeline.cache.async.withSource
import dif.tech.datapipeline.echoed.Echoed
import dif.tech.datapipeline.echoed.echoedLoadableCallResult
import dif.tech.datapipeline.rebuildable.Rebuildable
import dif.tech.datapipeline.rebuildable.StateFlowRebuildable
import dif.tech.datapipeline.shared_cache.SharedCache
import dif.tech.datapipeline.source.Source
import dif.tech.datapipeline.source.Sourced
import dif.tech.datapipeline.utils.ArgumentedHolder
import dif.tech.datapipeline.utils.SharedOnceFlow
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*

class InteractorModel(
    modelScope: CoroutineScope,
    private val skeleton: InteractorSkeleton,
    val onCloseClick: () -> Unit,
    sharedCache: SharedCache,
    private val doRequest: suspend (author: Author) -> ResourceResult<Book, SimpleDomainError>,
) {

    val author: Author
        get() = skeleton.author

    val index: Int
        get() = skeleton.index

    private val _isLoading: MutableStateFlow<Boolean> = MutableStateFlow(false)

    val isLoading: StateFlow<Boolean>
        get() = _isLoading

    private val sharedValue = sharedCache.getCachedValue { cacheScope ->
        ArgumentedHolder { author: Author ->
            SharedValue(
                cache = FileAccessor(
                    file = File("${author.name}_book.json"),
                    stringMapper = Json.toMapper(Timestamped.serializer(Book.serializer())),
                )
                    .applyLifetime(1.minutes)
                    .tagged(cachedTag)
                    .toCache(cacheScope),
                loader = SharedOnceFlow(cacheScope) { doRequest(author) },
            )
        }
    }[author]

    private val rebuildableBook: Rebuildable<Loadable<ResourceResult<Sourced<Book>, SimpleDomainError>>> =
        StateFlowRebuildable(
            scope = modelScope,
        ) { rebuildableScope, version ->
            sharedValue
                .cache
                .withSource(
                    scope = rebuildableScope,
                    source = sharedValue
                        .loader
                        .onStart { _isLoading.value = true }
                        .onEach { _isLoading.value = false },
                    adapter = CacheSourceResultAdapter
                        .timestamped<Book>()
                        .clearTimestamp()
                        .sourced(
                            cachedTag = cachedTag,
                            instanceTag = UUID.randomUUID(),
                        )
                        .run {
                            when (author) {
                                Author.A -> this
                                Author.B -> filterResultSources(Source.New, Source.Cached)
                            }
                        }
                        .resourceResult(),
                    collectSourceWaiter = when (version) {
                        0 -> CollectSourceWaiter.timeDelta(15.seconds) { it.value.timestamp }
                        else -> CollectSourceWaiter.immediate()
                    },
                )
        }

    val book: StateFlow<Echoed<Loadable<ResourceResult<Sourced<Book>, SimpleDomainError>>, Sourced<Book>>> =
        rebuildableBook.state.echoedLoadableCallResult(modelScope)

    fun onRefreshClick() {
        rebuildableBook.rebuild()
    }

    companion object {

        private val cachedTag = UUID.randomUUID()
    }

}