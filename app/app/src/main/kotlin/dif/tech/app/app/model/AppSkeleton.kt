package dif.tech.app.app.model

import dif.tech.app.app.model.interactor.InteractorSkeletonSerializable
import kotlinx.coroutines.flow.MutableStateFlow

interface AppSkeleton {

    val interactors: MutableStateFlow<List<InteractorSkeletonSerializable>>

    var lastInteractorIndex: Int
}