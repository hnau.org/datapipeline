package dif.tech.app.app.model.interactor

import dif.tech.app.app.model.interactor.value.Author

interface InteractorSkeleton {
    val author: Author
    val index: Int
}