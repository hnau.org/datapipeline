package dif.tech.app.app

import dif.tech.common.kotlin.Loadable
import dif.tech.common.kotlin.Tagged
import dif.tech.common.kotlin.Timestamped
import dif.tech.common.kotlin.days
import dif.tech.common.kotlin.mapper.toMapper
import dif.tech.common.kotlin.minutes
import dif.tech.common.plata.BaseDomainError
import dif.tech.common.plata.ResourceResult
import dif.tech.datapipeline.cache.async.AsyncCache
import dif.tech.datapipeline.cache.async.accessor.FileAccessor
import dif.tech.datapipeline.cache.async.accessor.applyLifetime
import dif.tech.datapipeline.cache.async.accessor.tagged
import dif.tech.datapipeline.cache.async.accessor.toCache
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.CacheSourceResultAdapter
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.clearTimestamp
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.filterResultSources
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.resourceResult
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.sourced
import dif.tech.datapipeline.cache.async.cache_source_result_adapter.timestamped
import dif.tech.datapipeline.cache.async.collect_source_waiter.CollectSourceWaiter
import dif.tech.datapipeline.cache.async.collect_source_waiter.immediate
import dif.tech.datapipeline.cache.async.collect_source_waiter.timeDelta
import dif.tech.datapipeline.cache.async.withSource
import dif.tech.datapipeline.echoed.Echoed
import dif.tech.datapipeline.echoed.echoedLoadableCallResult
import dif.tech.datapipeline.rebuildable.StateFlowRebuildable
import dif.tech.datapipeline.shared_cache.SharedCache
import dif.tech.datapipeline.source.Source
import dif.tech.datapipeline.source.Sourced
import dif.tech.datapipeline.utils.ArgumentedHolder
import dif.tech.datapipeline.utils.SharedOnceFlow
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*

enum class Author { A, B }

@Serializable
data class Book(
    val title: String,
)

suspend fun loadBook(
    author: Author,
): ResourceResult<Book, BaseDomainError> {
    delay(1000)
    val book = Book(title = "Book of author $author")
    return ResourceResult.Result(book)
}

class Repository(
    scope: CoroutineScope,
    author: Author,
    sharedCache: SharedCache,
) {

    private data class Shared(
        val cache: AsyncCache<Tagged<Timestamped<Book>, UUID?>>,
        val bookLoader: Flow<ResourceResult<Book, BaseDomainError>>,
    )

    private val shared: Shared = sharedCache.getCachedValue { cacheScope: CoroutineScope ->
        ArgumentedHolder<Author, Shared> { authorLocal ->
            Shared(
                cache = FileAccessor(
                    file = File("book_of_author_$authorLocal.json"),
                    stringMapper = Json.toMapper(Timestamped.serializer(Book.serializer())),
                )
                    .applyLifetime(1.days)
                    .tagged<_, UUID?>(null)
                    .toCache(cacheScope),
                bookLoader = SharedOnceFlow(cacheScope) { loadBook(author) },
            )
        }
    }[author]

    private val rebuildableBook = StateFlowRebuildable(
        scope = scope,
    ) { rebuilldableScope, version ->
        shared.cache.withSource(
            scope = rebuilldableScope,
            source = shared.bookLoader,
            adapter = CacheSourceResultAdapter
                .timestamped<Book>()
                .clearTimestamp()
                .sourced(
                    cachedTag = null,
                    instanceTag = UUID.randomUUID(),
                )
                .run {
                    when (author) {
                        Author.A -> this
                        Author.B -> filterResultSources(Source.Cached, Source.New)
                    }
                }
                .resourceResult(),
            collectSourceWaiter = when (version) {
                0 -> CollectSourceWaiter.timeDelta(5.minutes) { it.value.timestamp }
                else -> CollectSourceWaiter.immediate()
            },
        )
    }

    val book: StateFlow<Echoed<Loadable<ResourceResult<Sourced<Book>, BaseDomainError>>, Sourced<Book>>> =
        rebuildableBook.state.echoedLoadableCallResult(scope)

    fun refresh() {
        rebuildableBook.rebuild()
    }
}

class ViewModel(
    private val author: Author,
    scope: CoroutineScope,
    sharedCache: SharedCache,
) {

    private val repository = Repository(
        scope = scope,
        author = author,
        sharedCache = sharedCache,
    )

    init {

        //Collect
        scope.launch {
            repository.book.collect(::printEchoedLoadableBookOrError)
        }

        //Pull to refresh
        repository.refresh()
    }

    private val Sourced<Book>.logString: String
        get() = "$value from $source"

    private fun printEchoedLoadableBookOrError(
        echoedLoadableBookOrError: Echoed<Loadable<ResourceResult<Sourced<Book>, BaseDomainError>>, Sourced<Book>>,
    ) {
        val echoedBook = echoedLoadableBookOrError.echoValue.getOrNull()
        if (echoedBook != null) {
            log("Last receivered book: ${echoedBook.logString}")
        }
        printLoadableBookOrError(echoedLoadableBookOrError.value)
    }

    private fun printLoadableBookOrError(
        loadableBookOrError: Loadable<ResourceResult<Sourced<Book>, BaseDomainError>>,
    ) {
        when (loadableBookOrError) {
            Loadable.Loading -> log("Cache state is loading")
            is Loadable.Ready -> printBookOrError(loadableBookOrError.value)
        }
    }

    private fun printBookOrError(
        bookOrError: ResourceResult<Sourced<Book>, BaseDomainError>,
    ) {
        when (bookOrError) {
            is ResourceResult.Error -> log("Unable load book. Error: ${bookOrError.message}")
            is ResourceResult.Result -> log("Book: ${bookOrError.result.logString}")
        }
    }

    private fun log(
        message: String,
    ) {
        println("[Author $author] $message")
    }
}

