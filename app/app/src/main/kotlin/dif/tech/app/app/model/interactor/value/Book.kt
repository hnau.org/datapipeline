package dif.tech.app.app.model.interactor.value

import kotlinx.serialization.Serializable

@Serializable
data class Book(
    val title: String,
)