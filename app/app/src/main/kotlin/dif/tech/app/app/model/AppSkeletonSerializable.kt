package dif.tech.app.app.model

import dif.tech.app.app.model.interactor.InteractorSkeletonSerializable
import dif.tech.common.kotlin.serialization.MutableStateFlowSerializer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.Serializable

@Serializable
class AppSkeletonSerializable(
    @Serializable(MutableStateFlowSerializer::class)
    override val interactors: MutableStateFlow<List<InteractorSkeletonSerializable>> = MutableStateFlow(emptyList()),
    override var lastInteractorIndex: Int = 0,
) : AppSkeleton