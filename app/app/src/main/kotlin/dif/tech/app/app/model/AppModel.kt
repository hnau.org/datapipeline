package dif.tech.app.app.model

import dif.tech.app.app.model.interactor.InteractorModel
import dif.tech.app.app.model.interactor.InteractorSkeletonSerializable
import dif.tech.app.app.model.interactor.value.Author
import dif.tech.app.app.model.interactor.value.Book
import dif.tech.common.kotlin.Scoped
import dif.tech.common.kotlin.createChild
import dif.tech.common.kotlin.state.mapStateLite
import dif.tech.common.kotlin.state.runningFoldState
import dif.tech.common.kotlin.state.cacheStateInScope
import dif.tech.common.plata.DomainError
import dif.tech.common.plata.ResourceResult
import dif.tech.common.plata.SimpleDomainError
import dif.tech.datapipeline.shared_cache.SharedCache
import dif.tech.datapipeline.shared_cache.SharedCacheImpl
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

data class AppModel(
    private val modelScope: CoroutineScope,
    private val skeleton: AppSkeleton,
) {

    private val sharedCache: SharedCache = SharedCacheImpl(modelScope)

    private val _isAuthorChooseDialogVisible = MutableStateFlow(false)

    val isAuthorChooseDialogVisible: StateFlow<Boolean>
        get() = _isAuthorChooseDialogVisible

    fun onAuthorChooseDialogDismiss() {
        _isAuthorChooseDialogVisible.value = false
    }

    fun onAuthorChoose(
        author: Author,
    ) {
        _isAuthorChooseDialogVisible.value = false
        skeleton.interactors.update { interactorSkeletons ->
            val index = skeleton.lastInteractorIndex + 1
            skeleton.lastInteractorIndex = index
            interactorSkeletons + InteractorSkeletonSerializable(
                index = index,
                author = author,
            )
        }
    }

    fun onCreateNewInteractorClick() {
        _isAuthorChooseDialogVisible.value = true
    }

    private val bookRequestMutex = Mutex()

    private val newBookRequestCallback_: MutableStateFlow<CompletableDeferred<NewBookRequestResult>?> =
        MutableStateFlow(null)

    val newBookRequestCallback: StateFlow<CompletableDeferred<NewBookRequestResult>?>
        get() = newBookRequestCallback_

    private var bookIndex = 0

    private suspend fun doBookRequest(
        author: Author,
    ): ResourceResult<Book, SimpleDomainError> = bookRequestMutex.withLock {
        bookIndex++
        val deferred = CompletableDeferred<NewBookRequestResult>()
        newBookRequestCallback_.value = deferred
        val result = deferred.await()
        newBookRequestCallback_.value = null
        when (result) {
            NewBookRequestResult.Success ->
                ResourceResult.Result(
                    Book(
                        title = "Book $bookIndex"
                    )
                )

            NewBookRequestResult.Error ->
                DomainError(SimpleDomainError("Unable load $bookIndex book of author $author"))
        }
    }

    val interactors: StateFlow<List<InteractorModel>> = run {

        fun InteractorSkeletonSerializable.createScopedModel(): Scoped<Pair<InteractorSkeletonSerializable, InteractorModel>> {
            val interactorModelScope = modelScope.createChild(
                createChildJob = ::SupervisorJob,
            )
            val appSkeleton = this@AppModel.skeleton
            val skeleton: InteractorSkeletonSerializable = this@createScopedModel
            return Scoped(
                scope = interactorModelScope,
                value = this to InteractorModel(
                    modelScope = interactorModelScope,
                    skeleton = skeleton,
                    onCloseClick = {
                        appSkeleton.interactors.update { interactorSkeletons ->
                            interactorSkeletons.filter { it !== skeleton }
                        }
                    },
                    sharedCache = sharedCache,
                    doRequest = ::doBookRequest,
                ),
            )
        }

        skeleton
            .interactors
            .runningFoldState(
                scope = modelScope,
                createInitial = { skeletonsStack ->
                    skeletonsStack.map { skeleton ->
                        skeleton.createScopedModel()
                    }
                },
                operation = { modelsStack, newSkeletonsStack ->
                    val availableModelsFromPreviousStack = modelsStack
                        .associateBy { scopedModel ->
                            scopedModel.value.first
                        }
                        .toMutableMap()
                    val result = newSkeletonsStack
                        .map { skeleton ->
                            availableModelsFromPreviousStack
                                .remove(skeleton)
                                ?: skeleton.createScopedModel()
                        }
                    availableModelsFromPreviousStack.forEach { (_, notReusedModel) ->
                        notReusedModel.scope.cancel()
                    }
                    result
                },
            )
            .mapStateLite { skeletonsWithModelsStack ->
                skeletonsWithModelsStack.map { it.value.second }
            }
            .cacheStateInScope(modelScope)
    }

    sealed interface NewBookRequestResult {

        data object Success : NewBookRequestResult

        data object Error : NewBookRequestResult
    }
}