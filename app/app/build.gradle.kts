plugins {
    id("hnau.kotlin.lib")
    alias(libs.plugins.kotlin.serialization)
}

dependencies {
    implementation(libs.kotlin.serialization.core)
    implementation(libs.kotlin.serialization.json)
    implementation(libs.arrow.core)
    implementation(libs.kotlin.coroutines.core)
    implementation(project(mapOf("path" to ":common:app")))
    implementation(project(mapOf("path" to ":common:kotlin")))
    implementation(project(mapOf("path" to ":datapipeline")))
    implementation(project(mapOf("path" to ":common:plata")))
}