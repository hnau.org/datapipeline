plugins {
    id("hnau.kotlin.lib")
    alias(libs.plugins.compose.desktop)
}


dependencies {
    implementation(libs.arrow.core)
    implementation(libs.rfcfacil)
    implementation(compose.desktop.currentOs)
    implementation(project(mapOf("path" to ":app:app")))
    implementation(project(mapOf("path" to ":app:compose")))
    implementation(project(mapOf("path" to ":common:app")))
}


compose.desktop {
    application {
        mainClass = "dif.tech.app.desktop.DesktopAppKt"
    }
}