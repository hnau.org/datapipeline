package dif.tech.app.desktop

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.InternalComposeApi
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.LocalSystemTheme
import androidx.compose.ui.SystemTheme
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import dif.tech.app.app.App
import dif.tech.app.compose.Show

@OptIn(InternalComposeApi::class)
fun main() = application {
    val scale = 2.5f
    val appScope = rememberCoroutineScope()
    val app = App(
        scope = appScope,
        savedState = null,
    )
    Window(
        onCloseRequest = { exitApplication() },
        title = "DataPipeline",
        state = rememberWindowState(width = 720.dp * scale, height = 640.dp * scale),
        icon = rememberVectorPainter(Icons.Default.Add),
    ) {
        CompositionLocalProvider(
            LocalDensity provides Density(scale),
            LocalSystemTheme provides SystemTheme.Dark,
        ) {
            app.Show()
        }
    }
}