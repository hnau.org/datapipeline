package dif.tech.common.compose.uikit.utils

import androidx.compose.material.ContentAlpha

val ContentAlpha.almostTransparent get() = 0.12f
