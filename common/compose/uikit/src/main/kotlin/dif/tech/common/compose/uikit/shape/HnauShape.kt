package dif.tech.common.compose.uikit.shape

import androidx.compose.runtime.Immutable
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.toRect
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

@Immutable
class HnauShape(
  private val startTopRadiusPercentage: Float = 1f,
  private val endTopRadiusPercentage: Float = 1f,
  private val startBottomRadiusPercentage: Float = 1f,
  private val endBottomRadiusPercentage: Float = 1f,
) : Shape {

  override fun createOutline(
    size: Size,
    layoutDirection: LayoutDirection,
    density: Density,
  ): Outline {
    val maxRadius = with(density) { maxRadius.toPx() }

    val leftTopRadius = CornerRadius(
      when (layoutDirection) {
        LayoutDirection.Ltr -> startTopRadiusPercentage
        LayoutDirection.Rtl -> endTopRadiusPercentage
      } * maxRadius,
    )

    val rightTopRadius = CornerRadius(
      when (layoutDirection) {
        LayoutDirection.Ltr -> endTopRadiusPercentage
        LayoutDirection.Rtl -> startTopRadiusPercentage
      } * maxRadius,
    )

    val leftBottomRadius = CornerRadius(
      when (layoutDirection) {
        LayoutDirection.Ltr -> startBottomRadiusPercentage
        LayoutDirection.Rtl -> endBottomRadiusPercentage
      } * maxRadius,
    )

    val rightBottomRadius = CornerRadius(
      when (layoutDirection) {
        LayoutDirection.Ltr -> endBottomRadiusPercentage
        LayoutDirection.Rtl -> startBottomRadiusPercentage
      } * maxRadius,
    )

    return Outline.Rounded(
      RoundRect(
        rect = size.toRect(),
        topLeft = leftTopRadius,
        topRight = rightTopRadius,
        bottomLeft = leftBottomRadius,
        bottomRight = rightBottomRadius,
      ),
    )
  }

  companion object {
    private val maxRadius = 8.dp
  }
}
