package dif.tech.common.compose.uikit.shape

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue

@Composable
fun HnauShape.Companion.create(
  startTopRoundCorners: Boolean,
  endTopRoundCorners: Boolean,
  startBottomRoundCorners: Boolean,
  endBottomRoundCorners: Boolean,
): HnauShape {

  val startTopRadiusPercentage by animateFloatAsState(
    targetValue = if (startTopRoundCorners) 1f else 0f,
  )
  val endTopRadiusPercentage by animateFloatAsState(
    targetValue = if (endTopRoundCorners) 1f else 0f,
  )
  val startBottomRadiusPercentage by animateFloatAsState(
    targetValue = if (startBottomRoundCorners) 1f else 0f,
  )
  val endBottomRadiusPercentage by animateFloatAsState(
    targetValue = if (endBottomRoundCorners) 1f else 0f,
  )

  return HnauShape(
    startTopRadiusPercentage = startTopRadiusPercentage,
    endTopRadiusPercentage = endTopRadiusPercentage,
    startBottomRadiusPercentage = startBottomRadiusPercentage,
    endBottomRadiusPercentage = endBottomRadiusPercentage,
  )
}
