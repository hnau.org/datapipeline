package dif.tech.common.compose.uikit.contentsize

enum class ContentSize {
  Small,
  Medium,
  Large,
  ;

  companion object {
    val default: ContentSize = Medium
  }
}
