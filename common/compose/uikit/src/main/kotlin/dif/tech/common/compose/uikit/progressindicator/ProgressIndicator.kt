package dif.tech.common.compose.uikit.progressindicator

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import dif.tech.common.compose.uikit.progressindicator.utils.RoundCapCircularProgressIndicator

@Composable
fun ProgressIndicator(
    modifier: Modifier = Modifier
) {
    RoundCapCircularProgressIndicator(
        modifier = modifier,
    )
}
