package dif.tech.common.compose.uikit.contentsize

import androidx.compose.runtime.staticCompositionLocalOf

val LocalContentSize =
  staticCompositionLocalOf { ContentSize.default }
