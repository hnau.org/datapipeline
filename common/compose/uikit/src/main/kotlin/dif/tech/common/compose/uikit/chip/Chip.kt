package dif.tech.common.compose.uikit.chip

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.material.LocalContentColor
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import dif.tech.common.compose.uikit.chip.utils.ChipSide
import dif.tech.common.compose.uikit.contentsize.ContentSize
import dif.tech.common.compose.uikit.contentsize.LocalContentSize
import dif.tech.common.compose.uikit.shape.HnauShape
import dif.tech.common.compose.uikit.TripleRow
import dif.tech.common.compose.uikit.utils.Dimens

@Composable
fun Chip(
  modifier: Modifier = Modifier,
  size: ContentSize = LocalContentSize.current,
  activeColor: Color = MaterialTheme.colors.primary,
  style: ChipStyle = LocalChipStyle.current,
  shape: Shape = HnauShape(),
  onClick: (() -> Unit)? = null,
  leading: (@Composable () -> Unit)? = null,
  trailing: (@Composable () -> Unit)? = null,
  content: @Composable () -> Unit,
) {
  val materialColors = MaterialTheme.colors
  val colors = remember(style, activeColor, materialColors) {
    style.resolveColors(
      activeColor = activeColor,
      colors = materialColors,
    )
  }
  TripleRow(
    contentAlignment = Alignment.Start,
    modifier = modifier
      .height(size.height)
      .border(
        width = Dimens.border,
        shape = shape,
        color = animateColorAsState(colors.border).value,
      )
      .clip(shape)
      .then(
        when (onClick != null) {
          true -> Modifier.clickable(onClick = onClick)
          else -> Modifier
        },
      )
      .background(
        color = animateColorAsState(colors.background).value,
      ),
    leading = {
      ChipSide(
        side = ChipSide.leading,
        size = size,
        contentColor = animateColorAsState(colors.leading).value,
        content = leading,
      )
    },
    content = {
      CompositionLocalProvider(
        LocalContentColor provides animateColorAsState(colors.contentWithTrailing).value,
        LocalTextStyle provides size.textStyle,
      ) {
        Box(
          contentAlignment = Alignment.Center,
        ) { content() }
      }
    },
    trailing = {
      ChipSide(
        side = ChipSide.trailing,
        size = size,
        contentColor = animateColorAsState(colors.contentWithTrailing).value,
        content = trailing,
      )
    },
  )
}
