package dif.tech.common.compose.uikit.chip

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import dif.tech.common.compose.uikit.contentsize.ContentSize

internal val ContentSize.height: Dp
  get() = when (this) {
    ContentSize.Small -> 32.dp
    ContentSize.Medium -> 40.dp
    ContentSize.Large -> 48.dp
  }

internal val ContentSize.textStyle: TextStyle
  @Composable
  get() = when (this) {
    ContentSize.Small -> MaterialTheme.typography.body2
    ContentSize.Medium -> MaterialTheme.typography.body2
    ContentSize.Large -> MaterialTheme.typography.body1
  }

internal val ContentSize.contentEdgeSeparation: Dp
  get() = when (this) {
    ContentSize.Small -> 12.dp
    ContentSize.Medium -> 12.dp
    ContentSize.Large -> 16.dp
  }

internal val ContentSize.contentSideContentSeparation: Dp
  get() = when (this) {
    ContentSize.Small -> 2.dp
    ContentSize.Medium -> 4.dp
    ContentSize.Large -> 6.dp
  }

internal val ContentSize.sideContentEdgeSeparation: Dp
  get() = when (this) {
    ContentSize.Small -> 6.dp
    ContentSize.Medium -> 8.dp
    ContentSize.Large -> 10.dp
  }

internal val ContentSize.sideContentSize: Dp
  get() = when (this) {
    ContentSize.Small -> 20.dp
    ContentSize.Medium -> 24.dp
    ContentSize.Large -> 26.dp
  }
