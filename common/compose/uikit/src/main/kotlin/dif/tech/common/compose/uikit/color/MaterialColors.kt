package dif.tech.common.compose.uikit.color

import androidx.compose.ui.graphics.Color

object MaterialColors

private val colors = listOf(
  MaterialList(
    MaterialHue.Red,
    MaterialLightness.V50 to 0xffffebee,
    MaterialLightness.V100 to 0xffffcdd2,
    MaterialLightness.V200 to 0xffef9a9a,
    MaterialLightness.V300 to 0xffe57373,
    MaterialLightness.V400 to 0xffef5350,
    MaterialLightness.V500 to 0xfff44336,
    MaterialLightness.V600 to 0xffe53935,
    MaterialLightness.V700 to 0xffd32f2f,
    MaterialLightness.V800 to 0xffc62828,
    MaterialLightness.V900 to 0xffb71c1c,
    MaterialLightness.A100 to 0xffff8a80,
    MaterialLightness.A200 to 0xffff5252,
    MaterialLightness.A400 to 0xffff1744,
    MaterialLightness.A700 to 0xffd50000,
  ),

  MaterialList(
    MaterialHue.Pink,
    MaterialLightness.V50 to 0xfffce4ec,
    MaterialLightness.V100 to 0xfff8bbd0,
    MaterialLightness.V200 to 0xfff48fb1,
    MaterialLightness.V300 to 0xfff06292,
    MaterialLightness.V400 to 0xffec407a,
    MaterialLightness.V500 to 0xffe91e63,
    MaterialLightness.V600 to 0xffd81b60,
    MaterialLightness.V700 to 0xffc2185b,
    MaterialLightness.V800 to 0xffad1457,
    MaterialLightness.V900 to 0xff880e4f,
    MaterialLightness.A100 to 0xffff80ab,
    MaterialLightness.A200 to 0xffff4081,
    MaterialLightness.A400 to 0xfff50057,
    MaterialLightness.A700 to 0xffc51162,
  ),

  MaterialList(
    MaterialHue.Purple,
    MaterialLightness.V50 to 0xfff3e5f5,
    MaterialLightness.V100 to 0xffe1bee7,
    MaterialLightness.V200 to 0xffce93d8,
    MaterialLightness.V300 to 0xffba68c8,
    MaterialLightness.V400 to 0xffab47bc,
    MaterialLightness.V500 to 0xff9c27b0,
    MaterialLightness.V600 to 0xff8e24aa,
    MaterialLightness.V700 to 0xff7b1fa2,
    MaterialLightness.V800 to 0xff6a1b9a,
    MaterialLightness.V900 to 0xff4a148c,
    MaterialLightness.A100 to 0xffea80fc,
    MaterialLightness.A200 to 0xffe040fb,
    MaterialLightness.A400 to 0xffd500f9,
    MaterialLightness.A700 to 0xffaa00ff,
  ),

  MaterialList(
    MaterialHue.DeepPurple,
    MaterialLightness.V50 to 0xffede7f6,
    MaterialLightness.V100 to 0xffd1c4e9,
    MaterialLightness.V200 to 0xffb39ddb,
    MaterialLightness.V300 to 0xff9575cd,
    MaterialLightness.V400 to 0xff7e57c2,
    MaterialLightness.V500 to 0xff673ab7,
    MaterialLightness.V600 to 0xff5e35b1,
    MaterialLightness.V700 to 0xff512da8,
    MaterialLightness.V800 to 0xff4527a0,
    MaterialLightness.V900 to 0xff311b92,
    MaterialLightness.A100 to 0xffb388ff,
    MaterialLightness.A200 to 0xff7c4dff,
    MaterialLightness.A400 to 0xff651fff,
    MaterialLightness.A700 to 0xff6200ea,
  ),

  MaterialList(
    MaterialHue.Indigo,
    MaterialLightness.V50 to 0xffe8eaf6,
    MaterialLightness.V100 to 0xffc5cae9,
    MaterialLightness.V200 to 0xff9fa8da,
    MaterialLightness.V300 to 0xff7986cb,
    MaterialLightness.V400 to 0xff5c6bc0,
    MaterialLightness.V500 to 0xff3f51b5,
    MaterialLightness.V600 to 0xff3949ab,
    MaterialLightness.V700 to 0xff303f9f,
    MaterialLightness.V800 to 0xff283593,
    MaterialLightness.V900 to 0xff1a237e,
    MaterialLightness.A100 to 0xff8c9eff,
    MaterialLightness.A200 to 0xff536dfe,
    MaterialLightness.A400 to 0xff3d5afe,
    MaterialLightness.A700 to 0xff304ffe,
  ),

  MaterialList(
    MaterialHue.Blue,
    MaterialLightness.V50 to 0xffe3f2fd,
    MaterialLightness.V100 to 0xffbbdefb,
    MaterialLightness.V200 to 0xff90caf9,
    MaterialLightness.V300 to 0xff64b5f6,
    MaterialLightness.V400 to 0xff42a5f5,
    MaterialLightness.V500 to 0xff2196f3,
    MaterialLightness.V600 to 0xff1e88e5,
    MaterialLightness.V700 to 0xff1976d2,
    MaterialLightness.V800 to 0xff1565c0,
    MaterialLightness.V900 to 0xff0d47a1,
    MaterialLightness.A100 to 0xff82b1ff,
    MaterialLightness.A200 to 0xff448aff,
    MaterialLightness.A400 to 0xff2979ff,
    MaterialLightness.A700 to 0xff2962ff,
  ),

  MaterialList(
    MaterialHue.LightBlue,
    MaterialLightness.V50 to 0xffe1f5fe,
    MaterialLightness.V100 to 0xffb3e5fc,
    MaterialLightness.V200 to 0xff81d4fa,
    MaterialLightness.V300 to 0xff4fc3f7,
    MaterialLightness.V400 to 0xff29b6f6,
    MaterialLightness.V500 to 0xff03a9f4,
    MaterialLightness.V600 to 0xff039be5,
    MaterialLightness.V700 to 0xff0288d1,
    MaterialLightness.V800 to 0xff0277bd,
    MaterialLightness.V900 to 0xff01579b,
    MaterialLightness.A100 to 0xff80d8ff,
    MaterialLightness.A200 to 0xff40c4ff,
    MaterialLightness.A400 to 0xff00b0ff,
    MaterialLightness.A700 to 0xff0091ea,
  ),

  MaterialList(
    MaterialHue.Cyan,
    MaterialLightness.V50 to 0xffe0f7fa,
    MaterialLightness.V100 to 0xffb2ebf2,
    MaterialLightness.V200 to 0xff80deea,
    MaterialLightness.V300 to 0xff4dd0e1,
    MaterialLightness.V400 to 0xff26c6da,
    MaterialLightness.V500 to 0xff00bcd4,
    MaterialLightness.V600 to 0xff00acc1,
    MaterialLightness.V700 to 0xff0097a7,
    MaterialLightness.V800 to 0xff00838f,
    MaterialLightness.V900 to 0xff006064,
    MaterialLightness.A100 to 0xff84ffff,
    MaterialLightness.A200 to 0xff18ffff,
    MaterialLightness.A400 to 0xff00e5ff,
    MaterialLightness.A700 to 0xff00b8d4,
  ),

  MaterialList(
    MaterialHue.Teal,
    MaterialLightness.V50 to 0xffe0f2f1,
    MaterialLightness.V100 to 0xffb2dfdb,
    MaterialLightness.V200 to 0xff80cbc4,
    MaterialLightness.V300 to 0xff4db6ac,
    MaterialLightness.V400 to 0xff26a69a,
    MaterialLightness.V500 to 0xff009688,
    MaterialLightness.V600 to 0xff00897b,
    MaterialLightness.V700 to 0xff00796b,
    MaterialLightness.V800 to 0xff00695c,
    MaterialLightness.V900 to 0xff004d40,
    MaterialLightness.A100 to 0xffa7ffeb,
    MaterialLightness.A200 to 0xff64ffda,
    MaterialLightness.A400 to 0xff1de9b6,
    MaterialLightness.A700 to 0xff00bfa5,
  ),

  MaterialList(
    MaterialHue.Green,
    MaterialLightness.V50 to 0xffe8f5e9,
    MaterialLightness.V100 to 0xffc8e6c9,
    MaterialLightness.V200 to 0xffa5d6a7,
    MaterialLightness.V300 to 0xff81c784,
    MaterialLightness.V400 to 0xff66bb6a,
    MaterialLightness.V500 to 0xff4caf50,
    MaterialLightness.V600 to 0xff43a047,
    MaterialLightness.V700 to 0xff388e3c,
    MaterialLightness.V800 to 0xff2e7d32,
    MaterialLightness.V900 to 0xff1b5e20,
    MaterialLightness.A100 to 0xffb9f6ca,
    MaterialLightness.A200 to 0xff69f0ae,
    MaterialLightness.A400 to 0xff00e676,
    MaterialLightness.A700 to 0xff00c853,
  ),

  MaterialList(
    MaterialHue.LightGreen,
    MaterialLightness.V50 to 0xfff1f8e9,
    MaterialLightness.V100 to 0xffdcedc8,
    MaterialLightness.V200 to 0xffc5e1a5,
    MaterialLightness.V300 to 0xffaed581,
    MaterialLightness.V400 to 0xff9ccc65,
    MaterialLightness.V500 to 0xff8bc34a,
    MaterialLightness.V600 to 0xff7cb342,
    MaterialLightness.V700 to 0xff689f38,
    MaterialLightness.V800 to 0xff558b2f,
    MaterialLightness.V900 to 0xff33691e,
    MaterialLightness.A100 to 0xffccff90,
    MaterialLightness.A200 to 0xffb2ff59,
    MaterialLightness.A400 to 0xff76ff03,
    MaterialLightness.A700 to 0xff64dd17,
  ),

  MaterialList(
    MaterialHue.Lime,
    MaterialLightness.V50 to 0xfff9fbe7,
    MaterialLightness.V100 to 0xfff0f4c3,
    MaterialLightness.V200 to 0xffe6ee9c,
    MaterialLightness.V300 to 0xffdce775,
    MaterialLightness.V400 to 0xffd4e157,
    MaterialLightness.V500 to 0xffcddc39,
    MaterialLightness.V600 to 0xffc0ca33,
    MaterialLightness.V700 to 0xffafb42b,
    MaterialLightness.V800 to 0xff9e9d24,
    MaterialLightness.V900 to 0xff827717,
    MaterialLightness.A100 to 0xfff4ff81,
    MaterialLightness.A200 to 0xffeeff41,
    MaterialLightness.A400 to 0xffc6ff00,
    MaterialLightness.A700 to 0xffaeea00,
  ),

  MaterialList(
    MaterialHue.Yellow,
    MaterialLightness.V50 to 0xfffffde7,
    MaterialLightness.V100 to 0xfffff9c4,
    MaterialLightness.V200 to 0xfffff59d,
    MaterialLightness.V300 to 0xfffff176,
    MaterialLightness.V400 to 0xffffee58,
    MaterialLightness.V500 to 0xffffeb3b,
    MaterialLightness.V600 to 0xfffdd835,
    MaterialLightness.V700 to 0xfffbc02d,
    MaterialLightness.V800 to 0xfff9a825,
    MaterialLightness.V900 to 0xfff57f17,
    MaterialLightness.A100 to 0xffffff8d,
    MaterialLightness.A200 to 0xffffff00,
    MaterialLightness.A400 to 0xffffea00,
    MaterialLightness.A700 to 0xffffd600,
  ),

  MaterialList(
    MaterialHue.Amber,
    MaterialLightness.V50 to 0xfffff8e1,
    MaterialLightness.V100 to 0xffffecb3,
    MaterialLightness.V200 to 0xffffe082,
    MaterialLightness.V300 to 0xffffd54f,
    MaterialLightness.V400 to 0xffffca28,
    MaterialLightness.V500 to 0xffffc107,
    MaterialLightness.V600 to 0xffffb300,
    MaterialLightness.V700 to 0xffffa000,
    MaterialLightness.V800 to 0xffff8f00,
    MaterialLightness.V900 to 0xffff6f00,
    MaterialLightness.A100 to 0xffffe57f,
    MaterialLightness.A200 to 0xffffd740,
    MaterialLightness.A400 to 0xffffc400,
    MaterialLightness.A700 to 0xffffab00,
  ),

  MaterialList(
    MaterialHue.Orange,
    MaterialLightness.V50 to 0xfffff3e0,
    MaterialLightness.V100 to 0xffffe0b2,
    MaterialLightness.V200 to 0xffffcc80,
    MaterialLightness.V300 to 0xffffb74d,
    MaterialLightness.V400 to 0xffffa726,
    MaterialLightness.V500 to 0xffff9800,
    MaterialLightness.V600 to 0xfffb8c00,
    MaterialLightness.V700 to 0xfff57c00,
    MaterialLightness.V800 to 0xffef6c00,
    MaterialLightness.V900 to 0xffe65100,
    MaterialLightness.A100 to 0xffffd180,
    MaterialLightness.A200 to 0xffffab40,
    MaterialLightness.A400 to 0xffff9100,
    MaterialLightness.A700 to 0xffff6d00,
  ),

  MaterialList(
    MaterialHue.DeepOrange,
    MaterialLightness.V50 to 0xfffbe9e7,
    MaterialLightness.V100 to 0xffffccbc,
    MaterialLightness.V200 to 0xffffab91,
    MaterialLightness.V300 to 0xffff8a65,
    MaterialLightness.V400 to 0xffff7043,
    MaterialLightness.V500 to 0xffff5722,
    MaterialLightness.V600 to 0xfff4511e,
    MaterialLightness.V700 to 0xffe64a19,
    MaterialLightness.V800 to 0xffd84315,
    MaterialLightness.V900 to 0xffbf360c,
    MaterialLightness.A100 to 0xffff9e80,
    MaterialLightness.A200 to 0xffff6e40,
    MaterialLightness.A400 to 0xffff3d00,
    MaterialLightness.A700 to 0xffdd2c00,
  ),

  MaterialList(
    MaterialHue.Brown,
    MaterialLightness.V50 to 0xffefebe9,
    MaterialLightness.V100 to 0xffd7ccc8,
    MaterialLightness.V200 to 0xffbcaaa4,
    MaterialLightness.V300 to 0xffa1887f,
    MaterialLightness.V400 to 0xff8d6e63,
    MaterialLightness.V500 to 0xff795548,
    MaterialLightness.V600 to 0xff6d4c41,
    MaterialLightness.V700 to 0xff5d4037,
    MaterialLightness.V800 to 0xff4e342e,
    MaterialLightness.V900 to 0xff3e2723,
  ),

  MaterialList(
    MaterialHue.Grey,
    MaterialLightness.V50 to 0xfffafafa,
    MaterialLightness.V100 to 0xfff5f5f5,
    MaterialLightness.V200 to 0xffeeeeee,
    MaterialLightness.V300 to 0xffe0e0e0,
    MaterialLightness.V400 to 0xffbdbdbd,
    MaterialLightness.V500 to 0xff9e9e9e,
    MaterialLightness.V600 to 0xff757575,
    MaterialLightness.V700 to 0xff616161,
    MaterialLightness.V800 to 0xff424242,
    MaterialLightness.V900 to 0xff212121,
  ),

  MaterialList(
    MaterialHue.BlueGrey,
    MaterialLightness.V50 to 0xffeceff1,
    MaterialLightness.V100 to 0xffcfd8dc,
    MaterialLightness.V200 to 0xffb0bec5,
    MaterialLightness.V300 to 0xff90a4ae,
    MaterialLightness.V400 to 0xff78909c,
    MaterialLightness.V500 to 0xff607d8b,
    MaterialLightness.V600 to 0xff546e7a,
    MaterialLightness.V700 to 0xff455a64,
    MaterialLightness.V800 to 0xff37474f,
    MaterialLightness.V900 to 0xff263238,
  ),

).associateBy { materialList -> materialList.hue }

val MaterialColors.values
  get() = colors

operator fun MaterialColors.get(hue: MaterialHue) =
  values.getValue(hue)

val MaterialColors.red get() = get(MaterialHue.Red)
val MaterialColors.pink get() = get(MaterialHue.Pink)
val MaterialColors.purple get() = get(MaterialHue.Purple)
val MaterialColors.deepPurple get() = get(MaterialHue.DeepPurple)
val MaterialColors.indigo get() = get(MaterialHue.Indigo)
val MaterialColors.blue get() = get(MaterialHue.Blue)
val MaterialColors.lightBlue get() = get(MaterialHue.LightBlue)
val MaterialColors.cyan get() = get(MaterialHue.Cyan)
val MaterialColors.teal get() = get(MaterialHue.Teal)
val MaterialColors.green get() = get(MaterialHue.Green)
val MaterialColors.lightGreen get() = get(MaterialHue.LightGreen)
val MaterialColors.lime get() = get(MaterialHue.Lime)
val MaterialColors.yellow get() = get(MaterialHue.Yellow)
val MaterialColors.amber get() = get(MaterialHue.Amber)
val MaterialColors.orange get() = get(MaterialHue.Orange)
val MaterialColors.deepOrange get() = get(MaterialHue.DeepOrange)
val MaterialColors.brown get() = get(MaterialHue.Brown)
val MaterialColors.grey get() = get(MaterialHue.Grey)
val MaterialColors.blueGrey get() = get(MaterialHue.BlueGrey)

val Color.Companion.material
  get() = MaterialColors
