package dif.tech.common.compose.uikit.color

enum class MaterialLightness {
  V50, V100, V200, V300, V400, V500, V600, V700, V800, V900,
  A100, A200, A400, A700,
}
