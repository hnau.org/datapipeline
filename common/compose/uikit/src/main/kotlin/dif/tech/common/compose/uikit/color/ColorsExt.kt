package dif.tech.common.compose.uikit.color

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val Colors.foregroundLightness: MaterialLightness
  get() = when (isLight) {
    true -> MaterialLightness.V800
    false -> MaterialLightness.V400
  }

@Composable
operator fun Colors.get(
  hue: MaterialHue,
): Color = MaterialColors[hue][foregroundLightness]
