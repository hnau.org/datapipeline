plugins {
    id("hnau.kotlin.lib")
    alias(libs.plugins.compose.desktop)
}


dependencies {
    implementation(libs.arrow.core)
    implementation(libs.kotlin.coroutines.core)
    implementation(compose.desktop.currentOs)
    implementation(project(mapOf("path" to ":common:kotlin")))
    implementation(project(mapOf("path" to ":common:app")))
    implementation(project(mapOf("path" to ":common:compose:common")))
}