package dif.tech.common.compose.common

import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import arrow.core.Option
import arrow.core.Some
import dif.tech.common.kotlin.Mutable

@Composable
fun <T, L> AnimatedVisibility(
  value: T,
  toLocal: T.() -> Option<L>,
  modifier: Modifier = Modifier,
  enter: EnterTransition = fadeIn() + expandVertically(),
  exit: ExitTransition = fadeOut() + shrinkVertically(),
  label: String = "AnimatedVisibility",
  content: @Composable AnimatedVisibilityScope.(L) -> Unit,
) {
  val localOrNone = value.toLocal()
  androidx.compose.animation.AnimatedVisibility(
    visible = localOrNone.isDefined(),
    modifier = modifier,
    enter = enter,
    exit = exit,
    label = label,
  ) {
    var lastLocalOrNull by remember<Mutable<L?>> { Mutable(null) }
    if (localOrNone is Some<L>) {
      lastLocalOrNull = localOrNone.value
    }
    val lastLocal = lastLocalOrNull!!
    content(lastLocal)
  }
}
