plugins {
    id("hnau.kotlin.lib")
    alias(libs.plugins.compose.desktop)
}


dependencies {
    implementation(libs.kotlin.coroutines.core)
    implementation(libs.arrow.core)
    implementation(compose.desktop.currentOs)
    implementation(project(mapOf("path" to ":common:app")))
    implementation(project(mapOf("path" to ":common:kotlin")))
}