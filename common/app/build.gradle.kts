plugins {
    id("hnau.kotlin.lib")
    alias(libs.plugins.kotlin.serialization)
}

dependencies {
    implementation(libs.arrow.core)
    implementation(libs.kotlin.coroutines.core)
    implementation(libs.kotlin.serialization.core)
    implementation(project(mapOf("path" to ":common:kotlin")))
}
