package dif.tech.common.plata

typealias SimpleResourceResult<R> = ResourceResult<R, BaseDomainError>

sealed interface ResourceResult<out R, out E : BaseDomainError> {

  data class Result<out R>(
    val result: R,
  ) : ResourceResult<R, Nothing>

  sealed interface Error<out E : BaseDomainError> : ResourceResult<Nothing, E>, RequestError<E>
}
