package dif.tech.common.plata

sealed interface RequestError<out E : BaseDomainError> {

    val message: String?
    val reason: Throwable
}

data class DomainError<out E : BaseDomainError>(
    val domainError: E,
) : RequestError<E>, ResourceResult.Error<E> {

    override val message = domainError.message
    override val reason = Exception(message)
}

data class SystemError(
    override val reason: Throwable,
) : RequestError<Nothing>, ResourceResult.Error<Nothing> {

    override val message: String
        get() = reason.message.orEmpty()
}
