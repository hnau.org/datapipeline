package dif.tech.common.plata

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

fun <R, E : BaseDomainError> ResourceResult<R, E>.toOption(): Option<R> = when (this) {
    is ResourceResult.Error -> None
    is ResourceResult.Result -> Some(result)
}

inline fun <R, E : BaseDomainError, O> ResourceResult<R, E>.mapSuccess(
    successMapper: ((R) -> O),
): ResourceResult<O, E> =
    when (this) {
        is DomainError -> this
        is SystemError -> this
        is ResourceResult.Result -> ResourceResult.Result(successMapper.invoke(this.result))
    }

inline fun <R, E : BaseDomainError, O> ResourceResult<R, E>.mapResult(
    successMapper: ((R) -> O),
    errorMapper: ((Throwable) -> O),
): O =
    when (this) {
        is DomainError -> errorMapper.invoke(reason)
        is SystemError -> errorMapper.invoke(reason)
        is ResourceResult.Result -> successMapper.invoke(result)
    }

inline fun <R, E : BaseDomainError, O> ResourceResult<R, E>.mapResult(
    successMapper: ((R) -> O),
    domainErrorMapper: (E) -> O,
    systemErrorMapper: ((Throwable) -> O),
): O =
    when (this) {
        is DomainError -> domainErrorMapper.invoke(domainError)
        is SystemError -> systemErrorMapper.invoke(reason)
        is ResourceResult.Result -> successMapper.invoke(result)
    }

fun <R> ResourceResult<R, *>.getOrThrow(): R =
    when (this) {
        is ResourceResult.Error -> throw this.reason
        is ResourceResult.Result -> this.result
    }

fun <R> ResourceResult<R, *>.getOrNull(): R? =
    when (this) {
        is ResourceResult.Error -> null
        is ResourceResult.Result -> this.result
    }

val <R, E : BaseDomainError> ResourceResult<R, E>.isResult: Boolean
    get() = this is ResourceResult.Result

val <R, E : BaseDomainError> ResourceResult<R, E>.isError: Boolean
    get() = this is ResourceResult.Error
