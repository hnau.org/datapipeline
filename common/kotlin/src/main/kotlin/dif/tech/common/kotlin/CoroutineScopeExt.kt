package dif.tech.common.kotlin

import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.job

inline fun CoroutineScope.createChild(
  additionalContext: CoroutineContext = EmptyCoroutineContext,
  createChildJob: (parentJob: Job) -> Job,
) = CoroutineScope(
  context = coroutineContext + additionalContext + createChildJob(coroutineContext.job),
)
