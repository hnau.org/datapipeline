package dif.tech.common.kotlin

import arrow.core.raise.either
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

sealed interface Loadable<out T> {

  data object Loading : Loadable<Nothing>

  data class Ready<out T>(
    val value: T,
  ) : Loadable<T>

  companion object {

    inline fun <T> maxReady(
      first: Loadable<T>,
      second: Loadable<T>,
      combine: (T, T) -> T,
    ): Loadable<T> = first.fold(
      ifLoading = { second },
      ifReady = { firstValue ->
        second.fold(
          ifLoading = { first },
          ifReady = { secondValueValue ->
            val result = combine(firstValue, secondValueValue)
            Ready(result)
          },
        )
      },
    )
  }
}

inline fun <I, O> Loadable<I>.fold(
  ifLoading: () -> O,
  ifReady: (I) -> O,
): O = when (this) {
  Loadable.Loading -> ifLoading()
  is Loadable.Ready -> ifReady(value)
}

inline fun <I, O> Loadable<I>.flatMap(
  transform: (I) -> Loadable<O>,
): Loadable<O> = fold(
  ifLoading = { Loadable.Loading },
  ifReady = transform,
)

inline fun <I, O> Loadable<I>.map(
  transform: (I) -> O,
): Loadable<O> = flatMap { value ->
  val transformedValue = transform(value)
  Loadable.Ready(transformedValue)
}

fun <T> LoadableStateFlow(
  scope: CoroutineScope,
  operation: suspend () -> T,
): StateFlow<Loadable<T>> = MutableStateFlow<Loadable<T>>(
  value = Loadable.Loading,
).apply {
  scope.launch {
    val result = operation()
    value = Loadable.Ready(result)
  }
}
