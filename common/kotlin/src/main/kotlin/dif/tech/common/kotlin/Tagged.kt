package dif.tech.common.kotlin

data class Tagged<out V, out T>(
  val value: V,
  val tag: T,
) {

  inline fun <O> map(
    transform: (V) -> O,
  ): Tagged<O, T> = Tagged(
    value = transform(value),
    tag = tag,
  )
}
