package dif.tech.common.kotlin.mapper

import dif.tech.common.kotlin.castOrThrow


data class TypedConverter<I, O>(
    val type: Class<I>,
    val convert: (I) -> O,
)

@PublishedApi
internal inline fun <reified I, O> ((I) -> O).toTyped() = TypedConverter(
    type = I::class.java,
    convert = this,
)

data class TypedReverseMapper<I, O>(
    val direct: (I) -> O,
    val typedReverse: TypedConverter<O, I>,
)

inline fun <I, reified O> Mapper<I, O>.toTypedReverse() = TypedReverseMapper(
    direct = direct,
    typedReverse = reverse.toTyped(),
)

fun <I, K, M, O : Any> Mapper.Companion.typed(
    typesMappers: List<Pair<K, TypedReverseMapper<M, out O>>>,
    sourceToKeyAndMiddleSourceMapper: Mapper<I, Pair<K, M>>,
): Mapper<I, O> {
    val keyToDirectMap = typesMappers.associate { (key, mapper) ->
        key to mapper.direct
    }
    val classToKeyWithReverseMap = typesMappers.associate { (key, mapper) ->
        mapper.typedReverse.type to (key to mapper.typedReverse.convert)
    }
    return Mapper(
        direct = { source ->
            val (type, middleSource) = sourceToKeyAndMiddleSourceMapper.direct(source)
            val converter = keyToDirectMap.getValue(type)
            converter(middleSource)
        },
        reverse = { backSource ->
            val backSourceClass = backSource::class.java
            val (key, converter) = classToKeyWithReverseMap.getValue(backSourceClass)
            val middleSource = converter.castOrThrow<(O) -> M>()(backSource)
            sourceToKeyAndMiddleSourceMapper.reverse(key to middleSource)
        },
    )
}
