package dif.tech.common.kotlin.state

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transformLatest

inline fun <I, O> StateFlow<I>.mapStateLite(
    crossinline transform: (I) -> O,
): StateFlow<O> = this
    .transformLatest {
        emit(transform(it))
    }
    .toStateFlowLite { transform(value) }


@OptIn(ExperimentalCoroutinesApi::class)
fun <I, O> StateFlow<I>.flatMapStateLite(
    map: (I) -> StateFlow<O>,
): StateFlow<O> = this
    .flatMapLatest(map)
    .toStateFlowLite { map(value).value }