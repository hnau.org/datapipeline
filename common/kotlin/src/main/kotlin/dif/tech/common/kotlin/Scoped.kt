package dif.tech.common.kotlin

import kotlinx.coroutines.CoroutineScope

data class Scoped<T>(
  val scope: CoroutineScope,
  val value: T,
)

@Suppress("UnnecessaryLet")
inline fun <I, O> Scoped<I>.map(
  transform: (I) -> O,
): Scoped<O> = Scoped(
  scope = scope,
  value = value.let(transform),
)
