package dif.tech.common.kotlin

import kotlinx.serialization.Serializable

@Serializable
@JvmInline
value class Time(
    val milliseconds: Long,
) : Comparable<Time> {

    override fun toString() = "Timestamp(milliseconds=$milliseconds)"

    operator fun unaryPlus() = this
    operator fun unaryMinus() = Time(-milliseconds)

    operator fun plus(other: Time) = Time(this.milliseconds + other.milliseconds)
    operator fun minus(other: Time) = Time(this.milliseconds - other.milliseconds)
    operator fun times(count: Number) = Time((milliseconds * count.toDouble()).toLong())
    operator fun div(count: Number) = Time((milliseconds / count.toDouble()).toLong())
    operator fun div(other: Time) = milliseconds.toDouble() / other.milliseconds.toDouble()
    operator fun rem(count: Byte) = Time(milliseconds % count)
    operator fun rem(count: Short) = Time(milliseconds % count)
    operator fun rem(count: Int) = Time(milliseconds % count)
    operator fun rem(count: Long) = Time(milliseconds % count)
    operator fun rem(count: Time) = milliseconds % count.milliseconds

    override operator fun compareTo(other: Time) =
        milliseconds.compareTo(other.milliseconds)


    companion object {

        val zero = Time(0)
        val millisecond = Time(1)
        val second = millisecond * 1000
        val minute = second * 60
        val hour = minute * 60
        val day = hour * 24


        fun getNow(): Time =
            Time(System.currentTimeMillis())
    }
}

val Number.milliseconds: Time
    get() = Time.millisecond * this


val Number.seconds: Time
    get() = Time.second * this


val Number.minutes: Time
    get() = Time.minute * this


val Number.hours: Time
    get() = Time.hour * this


val Number.days: Time
    get() = Time.day * this