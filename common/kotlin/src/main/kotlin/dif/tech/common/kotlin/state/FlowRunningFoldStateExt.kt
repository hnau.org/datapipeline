package dif.tech.common.kotlin.state

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.runningFold
import kotlinx.coroutines.launch

fun <I, O> StateFlow<I>.runningFoldState(
    scope: CoroutineScope,
    createInitial: (I) -> O,
    operation: suspend (O, I) -> O,
): StateFlow<O> {
    val result = MutableStateFlow(createInitial(value))
    scope.launch {
        this@runningFoldState
            .runningFold(
                initial = Skippable(true, result.value),
                operation = { acc, value ->
                    Skippable(false, operation(acc.value, value))
                },
            )
            .filter { (skip) -> !skip }
            .map { (_, value) -> value }
            .collect(result)
    }
    return result
}

fun <T> StateFlow<T>.runningFoldState(
    scope: CoroutineScope,
    operation: suspend (T, T) -> T,
): StateFlow<T> = runningFoldState(
    scope = scope,
    createInitial = { it },
    operation = operation,
)

private data class Skippable<T>(
    val skip: Boolean,
    val value: T,
)
