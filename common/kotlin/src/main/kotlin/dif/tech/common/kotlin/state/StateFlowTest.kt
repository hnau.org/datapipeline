import dif.tech.common.kotlin.state.mapStateLite
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking

private fun log(
    tag: String,
    message: String,
) = println("[${System.currentTimeMillis()}] $tag $message")

fun main() = runBlocking {

    val source = MutableStateFlow("A")

    val result = source.mapStateLite {
        log("map", it)
        "$it-map"
    }

    delay(100)

    source.value = "B"

    delay(100)

    result.collect {
        log("collect", it)
    }

    Unit
}