package dif.tech.common.kotlin

import kotlinx.serialization.Serializable

@Serializable
data class Timestamped<T>(
    val value: T,
    val timestamp: Time,
) {

    inline fun <O> map(
        transform: (T) -> O,
    ): Timestamped<O> = Timestamped(
        value = transform(value),
        timestamp = timestamp,
    )

    companion object {

        fun <T> now(
            value: T,
        ): Timestamped<T> = Timestamped(
            value = value,
            timestamp = Time.getNow(),
        )
    }
}